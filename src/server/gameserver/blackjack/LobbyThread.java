package server.gameserver.blackjack;

import global.Player;

import java.io.*;
import java.net.Socket;

public class LobbyThread extends Thread
{
	private LobbyServer lobby;
	private Socket client;
	
	private ObjectOutputStream output;
	
	public LobbyThread(Socket client, LobbyServer lobby, ObjectOutputStream out, ObjectInputStream in)
	{
		super("LobbyThread");
		this.client = client;
		this.lobby = lobby;
		this.output = out;
		new LobbyListenThread(this,in).start();
	}
	
	public ObjectOutputStream getOutput()
	{return this.output;}
	public Socket getSocket()
	{return this.client;}
	public LobbyServer getLobby()
	{return this.lobby;}

}
