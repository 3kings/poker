package server.gameserver.blackjack;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class MainServer extends Thread
{
	private ArrayList<LobbyServer> lobbies = new ArrayList<LobbyServer>();
	private int starting = 4000;
	
	private ServerSocket serv;
	private Socket client;
	private ObjectInputStream input;
	private ObjectOutputStream output;

	public MainServer()
	{
		super("MainServer");
	}
	
	public void run()
	{
		try
		{
			serv = new ServerSocket(starting);
			serv.setSoTimeout(0);
			starting++;
			while(true)
			{
				client = serv.accept();
				input = new ObjectInputStream(client.getInputStream());
				output = new ObjectOutputStream(client.getOutputStream());
				int x = 0;
				for(int i = 0; i < lobbies.size(); i++)
				{
					if(lobbies.get(i) != null)
					{
						if(lobbies.get(i).getPlayers().size() <= 3)
						{
							output.writeObject(lobbies.get(i).getPort());
						}
						else
						{
							x++;
						}
					}
				}
				if(x == lobbies.size())
				{
					lobbies.add(new LobbyServer(new ServerSocket(starting), starting));
					lobbies.get(lobbies.size()-1).start();
					output.writeObject(starting);
					starting++;
				}
				x = 0;
			}
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	public ArrayList<LobbyServer> getAllLobbies()
	{return this.lobbies;}
	
	public static void main(String[] args)
	{
		//new MainServer().start();

	}

}
