package server.gameserver.blackjack;

import global.Cards;
import global.Player;

import java.io.ObjectInputStream;
import java.util.ArrayList;

import javax.swing.ImageIcon;

public class LobbyListenThread extends Thread
{
	private LobbyThread parent;
	private ObjectInputStream input;
	
	private boolean on = false;
	
	public LobbyListenThread(LobbyThread parent, ObjectInputStream input)
	{
		super("LobbyListenThread");
		this.parent = parent;
		on = true;
		this.input = input;
	}
	
	public void run()
	{
		ArrayList<Object> caught;
		try
		{
			while(on)
			{
				caught = (ArrayList<Object>) input.readObject();
				switch((String)caught.get(0))
				{
					case "player":
						parent.getLobby().sendPlayerUpdate((Player)caught.get(1));
						parent.getLobby().sendPotUpdate((Integer)caught.get(2));
						break;
					case "stay":
						Player my = (Player) caught.get(1);
						ArrayList<ImageIcon> cards1 = my.getHand();
						parent.getLobby().getHandCounts().add(getHandCount(cards1));
						if(parent.getLobby().getPlayers().size()-1 == parent.getLobby().getTurn())
							parent.getLobby().sendTurnUpdate(new Integer(-1),true);
						else
							parent.getLobby().sendTurnUpdate((parent.getLobby().getTurn()+1),false);
						break;
				}
			}
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	public Integer getHandCount(ArrayList<ImageIcon> cards1)
	{
		Integer count = 0;
		for(int i = 0; i < cards1.size(); i++)
			count += Cards.getFaceValue(cards1.get(i).getDescription());
		return count;
	}

}
