package server.gameserver.blackjack;

import global.Player;
import global.SerializableFilesManager;

import java.io.*;
import java.net.*;
import java.util.*;

public class LobbyServer extends Thread
{
	private Socket client;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	
	private ArrayList<LobbyThread> playerThreads = new ArrayList<LobbyThread>();
	private ArrayList<Player> players = new ArrayList<Player>();
	private ArrayList<Socket> clients = new ArrayList<Socket>();
	
	private ServerSocket serv;
	private Integer myPort;
	
	private ArrayList<Integer> hands = new ArrayList<Integer>();
	private Integer pot;
	private Integer lobbyTurn;
	
	public LobbyServer(ServerSocket serv, Integer myPort)
	{
		super("LobbyServer");
		this.serv = serv;
		this.myPort = myPort;
		lobbyTurn = new Integer(0);
		pot = new Integer(0);
	}
	
	public void run()
	{
		Object caught;
		Player newP;
		try
		{
			serv.setSoTimeout(0);
			while(true)
			{
				client = serv.accept();
				input = new ObjectInputStream(client.getInputStream());
				output = new ObjectOutputStream(client.getOutputStream());
				clients.add(client);
				caught = input.readObject();
				newP = SerializableFilesManager.loadPlayer((String)caught);
				output.writeObject(newP);
				
				output.writeObject(pot);
				output.writeObject(playerThreads.size());
				output.writeObject(lobbyTurn);
				
				players.add(newP);
				playerThreads.add(new LobbyThread(client,this,output,input));
				playerThreads.get(playerThreads.size()-1).start();
				this.sendAllPlayersToAll();
			}
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	public void sendPlayerUpdate(Player player)
	{
		try
		{
			addTo(player);
			ArrayList<Object> info = new ArrayList<Object>();
			info.add("player");
			info.add(player);
			for(int i = 0; i < playerThreads.size(); i++)
				playerThreads.get(i).getOutput().writeObject(info);
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	public void sendAllPlayersToAll()
	{
		try
		{
			for(int i = 0; i < players.size(); i++)
			{
				ArrayList<Object> info = new ArrayList<Object>();
				info.add("player");
				info.add(players.get(i));
				for(int x = 0; x < playerThreads.size(); x++)
				{
					playerThreads.get(x).getOutput().writeObject(info);
					playerThreads.get(x).getOutput().reset();
				}
			}
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	public void sendAllUpdatesToAllPlayers()
	{
		try
		{
			for(int i = 0; i < players.size(); i++)
			{
				ArrayList<Object> info = new ArrayList<Object>();
				info.add("player");
				info.add(players.get(i));
				for(int x = 0; x < playerThreads.size(); x++)
				{
					playerThreads.get(x).getOutput().writeObject(info);
					playerThreads.get(x).getOutput().reset();
				}
			}
			for(int x = 0; x < playerThreads.size(); x++)
			{
				ArrayList<Object> info = new ArrayList<Object>();
				info.add("pot");
				info.add(this.pot);
				playerThreads.get(x).getOutput().writeObject(info);
				playerThreads.get(x).getOutput().reset();
			}
			for(int x = 0; x < playerThreads.size(); x++)
			{
				ArrayList<Object> info = new ArrayList<Object>();
				info.add("turn");
				info.add(this.lobbyTurn);
				playerThreads.get(x).getOutput().writeObject(info);
				playerThreads.get(x).getOutput().reset();
			}
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	public void sendPotUpdate(Integer newPot)
	{
		try
		{
			this.pot = newPot;
			ArrayList<Object> info = new ArrayList<Object>();
			info.add("pot");
			info.add(newPot);
			for(int i = 0; i < playerThreads.size(); i++)
				playerThreads.get(i).getOutput().writeObject(info);
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	public void sendTurnUpdate(Integer newTurn, boolean win)
	{
		try
		{
			if(!win)
			{
				this.lobbyTurn = newTurn;
				ArrayList<Object> info = new ArrayList<Object>();
				info.add("turn");
				info.add(newTurn);
				for(int i = 0; i < playerThreads.size(); i++)
					playerThreads.get(i).getOutput().writeObject(info);
			}
			else
			{
				determineWinner();
				for(int i = 0; i < players.size(); i++)
				{
					ArrayList<Object> info = new ArrayList<Object>();
					info.add("player");
					info.add(players.get(i));
					for(int x = 0; x < playerThreads.size(); x++)
					{
						playerThreads.get(x).getOutput().writeObject(info);
					}
				}
			}
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	public void determineWinner()
	{
		int index = 0;
		Integer highest = hands.get(0);
		for(int i = 0; i < hands.size(); i++)
		{
			if(highest < hands.get(i) && hands.get(i) <= 21)
			{
				highest = hands.get(i);
				index = i;
			}
		}
		if(index == 0 && highest > 21)
			index = -1;
		if(index == -1)
		{
			reset();
			return;
		}
		players.get(index).setChips(this.pot+players.get(index).getChips());
		reset();
	}
	
	public void reset()
	{
		this.hands.clear();
		this.pot = 0;
		this.lobbyTurn = 0;
		for(int i = 0; i < players.size(); i++)
		{
			players.get(i).clearHand();
			SerializableFilesManager.savePlayer(players.get(i));
		}
		this.sendAllUpdatesToAllPlayers();
	}
	
	public Integer getTurn()
	{return this.lobbyTurn;}
	public void addPlayer(Player player)
	{this.players.add(player);}
	public ArrayList<Integer> getHandCounts()
	{return hands;};
	public Integer getPot()
	{return this.pot;}
	
	public void addTo(Player newInfo)
	{
		String name = newInfo.getName();
		Player temp;
		String compareName;
		int indexToChange = -1;
		for(int i = 0; i < players.size(); i++)
		{
			temp = players.get(i);
			compareName = temp.getName();
			if(compareName.equals(name))
			{
				indexToChange = i;
				break;
			}
		}
		if(indexToChange == -1)
			return;
		else
			players.set(indexToChange, newInfo);
	}
	
	public void removePlayer(Player toRemove)
	{
		String name = toRemove.getName();
		Player temp;
		String compareName;
		for(int i = 0; i < players.size(); i++)
		{
			temp = players.get(i);
			compareName = temp.getName();
			if(compareName.equals(name))
			{
				SerializableFilesManager.savePlayer(players.get(i));
				players.remove(i);
				return;
			}
		}
	}
	
	public ServerSocket getMyServer()
	{return this.serv;}
	public ArrayList<LobbyThread> getPlayerThreads()
	{return this.playerThreads;}
	public ArrayList<Player> getPlayers()
	{return this.players;}
	public int getPort()
	{return this.myPort;}
	
}
