package server.userpasserver;

import java.io.IOException;
import java.net.ServerSocket;

public class UserPassMultServer extends Thread
{
	public static boolean listening = true;
	
	public UserPassMultServer()
	{
		super("UserPassMultServer");
	}
	
	public void run()
	{
		ServerSocket serv = null;
		
		try{serv = new ServerSocket(5000);} 
		catch (IOException e)
		{e.printStackTrace();}
		
		while(listening)
			try
			{new UserPassServer(serv.accept()).start();} 
			catch (IOException e)
			{e.printStackTrace();}
	}

}
