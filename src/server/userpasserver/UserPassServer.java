package server.userpasserver;

import global.Player;
import global.SerializableFilesManager;
import global.dgframework.CredenDatagram;
import global.dgframework.Datagram;
import global.dgframework.DatagramHandler;

import java.io.*;
import java.net.*;
import java.util.*;

import javax.swing.ImageIcon;

import server.encrypt.Encrypter;

public class UserPassServer extends Thread
{
	
    private Socket clientSocket = null;
    private ObjectOutputStream objectOutput = null;
    private ObjectInputStream objectInput = null;
    
    private boolean running;
	
	public UserPassServer(Socket soc)
	{
		super("UserPassServer");
		this.clientSocket = soc;
		this.running = true;
	}

	//Checks to see if there is a file with the demoted "name" exists.
	public boolean containsUsername(String name)
	{
		return SerializableFilesManager.containsPlayer(name);
	}
	
	//FOR REGISTERING  ACCOUNTS ONLY. return true if it was successful and false if it was not (that means there is already that account)
	public void write(String user, String pass, String ip, ImageIcon icon)
	{
		try
		{
			if(containsUsername(user))
			{
				objectOutput.writeObject(new CredenDatagram(false));
			}
			else
			{
					SerializableFilesManager.savePlayer(new Player(user, 5000, ip, Encrypter.encypt(pass), icon));
					objectOutput.writeObject(new CredenDatagram(true));
			}
		}
		catch(Exception e)
		{
			System.err.println("Error IN UserPassServer (Saving players or writing back)");
			running = false;
		}
		running = false;
	}
	
	//compares username and password to everyone's and see if the person got their creds. right
	private boolean userIsRegistered(String name, String pass)
	{
		if(SerializableFilesManager.containsPlayer(name))
		{
			Player user = SerializableFilesManager.loadPlayer(name);
			if(user.getName().equalsIgnoreCase(name.replaceAll(" ", "")) && user.getPassword().equals(Encrypter.encypt(pass)))
				return true;
		}
		/*File folder = new File("data/players/");
		ArrayList<String> allPlayers = new ArrayList<String>();
		allPlayers.clear();
		for(File playerFile : folder.listFiles())
			allPlayers.add(playerFile.getName().replace(".p", ""));
		for(int i = 0; i < allPlayers.size(); i++)
			if(allPlayers.get(i) != null && SerializableFilesManager.loadPlayer(allPlayers.get(i)) != null)
				if(allPlayers.get(i).equalsIgnoreCase(name.replace(" ", "")) && SerializableFilesManager.loadPlayer(allPlayers.get(i)).getPassword().equals(Encrypter.encypt(pass.replace(" ", ""))))
					return true;*/
		return false;
	}
	
	//checks to see if the user is banned. They have a boolean set to themselves.
	private boolean userBanned(String name)
	{
		if(SerializableFilesManager.containsPlayer(name))
			if(SerializableFilesManager.loadPlayer(name).isBanned())
				return true;
		return false;
	}
	
	//checks to see if the user is online. They have a boolean set to themselves
	
	private boolean userOnline(String name)
	{
		if(SerializableFilesManager.containsPlayer(name))
			if(SerializableFilesManager.loadPlayer(name).isOnline())
				return true;
		return false;
	}
	
	public void checkCre(String user, String pass)
	{
		try
		{
			objectOutput.writeObject(new CredenDatagram(false, userIsRegistered(user,pass), userBanned(user), userOnline(user)));
		}
		catch(Exception e)
		{
			e.printStackTrace();
			running = false;
		}
		running = false;
	}
	
	public void run()
	{
        try
        {
	    	while(true)
	        {
	    		objectOutput = new ObjectOutputStream(clientSocket.getOutputStream());
	    		objectInput = new ObjectInputStream(clientSocket.getInputStream());
	    		DatagramHandler.parseDatagram((Datagram)objectInput.readObject(), this);
	         	break;
	        }
        }
        catch(Exception e)
        {running = false; e.printStackTrace();}
        finally
        {
        	if(running)
        		running = false;
            if(objectInput != null)
            {
            	try
            	{
            		objectInput.close();
            	}
            	catch(Exception ex)
            	{}
            }
            if (clientSocket != null)
            {
                try
                {
                    clientSocket.close();
                }
                catch (Exception ex){}
            }
        }
	}
}
