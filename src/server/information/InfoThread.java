package server.information;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

import global.MD5Hash;
import global.Player;
import global.SerializableFilesManager;
import global.dgframework.Datagram;
import global.dgframework.DatagramHandler;
import global.dgframework.MessageDatagram;
import global.dgframework.PlayersDatagram;
import global.dgframework.StatDatagram;
import global.dgframework.VerifyDatagram;
import server.utils.Fixer;
import server.utils.Notifications;

public class InfoThread extends Thread
{
	private Socket client;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	private InfoServer parent;
	
	private boolean running = false;
	
	public InfoThread(InfoServer parent, Socket client)
	{
		this.parent = parent;
		this.client = client;
		running = true;
	}
	
	public void run()
	{
		try
		{
			in = new ObjectInputStream(client.getInputStream());
			out = new ObjectOutputStream(client.getOutputStream());
			DatagramHandler.parseDatagram((Datagram)in.readObject(), this);
			while(running)
			{}
			in.close();
			out.flush();
			out.close();
			client.close();
		}
		catch(Exception e)
		{System.err.println("Error in \"InfoThread.java\"");e.printStackTrace();}
		finally
		{
			if(in != null)
			{
				try
				{in.close();}
				catch (IOException e)
				{e.printStackTrace();}
			}
			if(out != null)
			{
				try
				{out.close();}
				catch (IOException e)
				{e.printStackTrace();}
			}
		}
	}
	
	public void sendVert(String buffer)
	{
		try
		{
			File perm = new File(System.getProperty("user.dir")+"/GameClient.jar");
			if(MD5Hash.verify(buffer, MD5Hash.hash(perm)))
				out.writeObject(new VerifyDatagram(0));
			else
				out.writeObject(new VerifyDatagram(1));
			running = false;
		}
		catch(Exception e)
		{running = false; e.printStackTrace();}
		running = false;
	}
	
	public void sendStats(String username)
	{
		try
		{
			out.writeObject(new StatDatagram(SerializableFilesManager.loadPlayer(username)));
			running = false;
		}
		catch(Exception e)
		{running = false; e.printStackTrace();}
		running = false;
	}
	
	public void sendPlayers()
	{
		try
		{
			out.writeObject(new PlayersDatagram(Fixer.getAllPlayerNames()));
			running = false;
		}
		catch(Exception e)
		{running = false;e.printStackTrace();}
		running = false;
	}
	
	private String[] seperateUsers(String a)
	{
		if(a.contains(","))
			return a.split(",");
		return new String[]{a};
	}
	
	public void sendToAllPlayers(MessageDatagram gram)
	{
		String[] users = seperateUsers(gram.getUsers());
		ArrayList<String> message = new ArrayList<String>();
		message.add(gram.getFrom());
		message.add(gram.getSubject());
		message.add(gram.getMessage());
		Player loaded;
		Player loadedFrom = SerializableFilesManager.loadPlayer(gram.getFrom());
		if(loadedFrom.isAdmin() || loadedFrom.isMod())
		{
			if(users.length == 1 && users[0] != null && users[0].equals("ALL"))
			{
				ArrayList<String> players = Fixer.getAllPlayerNames();
				for(int i = 0; i < players.size(); i++)
				{
					if(players.get(i) != null)
					{
						if(SerializableFilesManager.containsPlayer(players.get(i)))
						{
							loaded = getPlayerInLobby(players.get(i));
							if(loaded != null)
							{
								if(loaded.getMessages() == null)
									loaded.constructMessages();
								loaded.addMessage(message);
								this.setPlayerInLobby(loaded);
								SerializableFilesManager.savePlayer(loaded);
								try
								{
									if(loaded.isOnline())
										new Notifications("You've Recieved a New Message", loaded.getIP()).start();
								}
								catch(Exception e)
								{e.printStackTrace();}
							}
							else
							{
								loaded = SerializableFilesManager.loadPlayer(players.get(i));
								if(loaded.getMessages() == null)
									loaded.constructMessages();
								loaded.addMessage(message);
								loaded.incrementMessageQue();
								SerializableFilesManager.savePlayer(loaded);
							}
						}
					}
				}
			}
		}
		for(String user : users)
		{
			if(user != null)
			{
				if(SerializableFilesManager.containsPlayer(user))
				{
					loaded = getPlayerInLobby(user);
					if(loaded != null)
					{
						if(loaded.getMessages() == null)
							loaded.constructMessages();
						loaded.addMessage(message);
						this.setPlayerInLobby(loaded);
						SerializableFilesManager.savePlayer(loaded);
						try
						{
							if(loaded.isOnline())
								new Notifications("You've Recieved a New Message", loaded.getIP()).start();
						}
						catch(Exception e)
						{e.printStackTrace();}
					}
					else
					{
						loaded = SerializableFilesManager.loadPlayer(user);
						if(loaded.getMessages() == null)
							loaded.constructMessages();
						loaded.addMessage(message);
						loaded.incrementMessageQue();
						SerializableFilesManager.savePlayer(loaded);
					}
				}
			}
		}
		try
		{
			out.writeObject(new MessageDatagram(null,"from",null,null));
			out.flush();
		}
		catch(Exception e)
		{e.printStackTrace();}
		running = false;
	}
	
	private Player getPlayerInLobby(String name)
	{
		ArrayList<Player> players = parent.getParent().getLobbyServer().getUsers();
		for(int i = 0; i < players.size(); i++)
			if(players.get(i) != null)
				if(players.get(i).getName().equalsIgnoreCase(name))
					return players.get(i);
		return null;
	}
	
	private void setPlayerInLobby(Player newSet)
	{
		ArrayList<Player> players = parent.getParent().getLobbyServer().getUsers();
		for(int i = 0; i < players.size(); i++)
			if(players.get(i) != null)
				if(players.get(i).getName().equalsIgnoreCase(newSet.getName()))
					players.set(i, newSet);
	}
	
	/*public boolean inBlackJack(Player a)
	{
		ArrayList<server.gameserver.blackjack.LobbyServer> lobbies = parent.getParent().getBlackJackServer().getAllLobbies();
		ArrayList<Player> players = null;
		for(server.gameserver.blackjack.LobbyServer c : lobbies)
		{
			if(c != null)
				players = c.getPlayers();
			if(players.size() == 0)
				continue;
			for(Player b : players)
				if(b != null)
					if(a.getName().equalsIgnoreCase(b.getName()))
						return true;
		}
		return false;
	}*/
	
}
