package server.information;

import java.net.ServerSocket;

import server.mainserver.MainServer;

public class InfoServer extends Thread
{
	private ServerSocket serv;
	private boolean on = false;
	private MainServer parent;
	
	public InfoServer(MainServer parent)
	{
		super("InfoServer");
		this.parent = parent;
		on = true;
	}
	
	public void run()
	{
		try
		{
			serv = new ServerSocket(5001);
			while(on)
			{
				new InfoThread(this, serv.accept()).start();
			}
		}
		catch(Exception e)
		{System.err.println("Error in InfoServer.java");}
	}
	
	public MainServer getParent()
	{return this.parent;}

}
