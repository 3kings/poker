package server.gameservers.texas;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class LobbyThread extends Thread
{
	private LobbyServer lobby;
	private Socket client;
	
	private ObjectOutputStream output;
	
	public LobbyThread(Socket client, LobbyServer lobby, ObjectOutputStream out, ObjectInputStream in)
	{
		this.lobby = lobby;
		this.client = client;
		this.output = out;
	}
	
	public void writeBooleans()
	{
		
	}
	
	public ObjectOutputStream getOutput()
	{return this.output;}
	public Socket getSocket()
	{return this.client;}
	public LobbyServer getLobby()
	{return this.lobby;}
}
