package server.gameservers.texas;

import global.Cards;
import global.Player;
import global.SerializableFilesManager;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.ImageIcon;

public class LobbyServer extends Thread
{
	private Socket client;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	
	private ArrayList<LobbyThread> playerThreads = new ArrayList<LobbyThread>(); //all of the playerThreads (output)
	private ArrayList<Player> players = new ArrayList<Player>(); //all of the players as players
	private ArrayList<Socket> clients = new ArrayList<Socket>(); //all of the players sockets
	
	private ServerSocket serv;
	private Integer myPort;
	
	
	private Cards card = new Cards(false); //used for generating a card
	private ArrayList<ImageIcon> cards = new ArrayList<ImageIcon>(); //server sided cards that are flipped Max is only 5
	private Integer pot; //the lobby pot which is updated after every raise/call
	private Integer lobbyTurn; //when someone simply checks or calls this will increase; when someone raises it will reset
	private Integer rounds = new Integer(0); //determine really what round they are on  (0,1,2,3) 0 = before flop 1 = after 2 = after first round of betting 3 = last round of betting
	private Boolean[][] rescritions = new Boolean[4][5];  //used to send to each player to determin what they can and cannot do (call,raise,check,fold,allin)
	private Integer[][] previousBets = new Integer[4][1]; //this is just for if people were to raise
	private Boolean[][] bet = new Boolean[4][1]; //this is if they have bet already that round (round is defined as a round of betting once finished then all booleans get reset)
	private Integer lobbyBet = new Integer(0); //keep track of what the bet is currently at (raises can hire it will be used with previousBets in case of a raise)
	
	
	public LobbyServer(ServerSocket serv, Integer myPort)
	{
		super("LobbyServer");
		this.serv = serv;
		this.myPort = myPort;
		lobbyTurn = new Integer(0);
		pot = new Integer(0);
	}
	
	public void run()
	{
		Object caught;
		Player newP;
		try
		{
			serv.setSoTimeout(0);
			while(true)
			{
				client = serv.accept();
				input = new ObjectInputStream(client.getInputStream());
				output = new ObjectOutputStream(client.getOutputStream());
				clients.add(client);
				caught = input.readObject();
				newP = SerializableFilesManager.loadPlayer((String)caught);
				
				//adding the two cards to his/her hand
				newP.clearHand();
				newP.addToHand(card.getRandomCard());
				newP.addToHand(card.getRandomCard());
				output.writeObject(newP);
				
				//need to write restrictions, lobby turn, their turn id, the pot, and the round it is on
				rescritions[playerThreads.size()] = new Boolean[5];
				previousBets[playerThreads.size()] = new Integer[1];
				bet[playerThreads.size()] = new Boolean[1];
				
				output.writeObject(rescritions[playerThreads.size()]);
				output.writeObject(lobbyTurn);
				output.writeObject(playerThreads.size());
				output.writeObject(pot);
				output.writeObject(rounds);
				
				players.add(newP);
				playerThreads.add(new LobbyThread(client,this,output,input));
				playerThreads.get(playerThreads.size()-1).start();
				this.sendAllPlayersToAll();
			}
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	public void sendAllPlayersToAll()
	{
		try
		{
			for(int i = 0; i < players.size(); i++)
			{
				ArrayList<Object> info = new ArrayList<Object>();
				info.add("player");
				info.add(players.get(i));
				for(LobbyThread a : playerThreads)
				{
					a.getOutput().writeObject(info);
					a.getOutput().reset();
				}
			}
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	public Integer getTurn()
	{return this.lobbyTurn;}
	public void addPlayer(Player player)
	{this.players.add(player);}
	public Integer getPot()
	{return this.pot;}
	
	public void resetBettingAndTurn()
	{
		lobbyTurn = new Integer(0);
		for(int i = 0; i < bet.length; i++)
			bet[i][0] = false;
	}
	
	private void reset()
	{
		card = new Cards(false);
		cards.clear();
		pot = new Integer(0);
		rounds = new Integer(0);
		resetRescritions();
		resetBets();
		resetBettingAndTurn();
		this.lobbyBet = new Integer(0);
	}
	
	public void resetBets()
	{
		for(int i = 0; i < previousBets.length; i++)
			previousBets[i][0] = new Integer(0);
	}
	
	public void resetBet()
	{
		for(int i = 0; i < bet.length; i++)
			bet[i][0] = new Boolean(false);
	}
	
	public void resetRescritions()
	{
		for(int i = 0; i < rescritions.length; i++)
			for(int x = 0; x < rescritions[i].length; x++)
				rescritions[i][x] = false;
	}
	
	public void setBetted(Player player, boolean bool)
	{
		int index = this.getPlayerIndex(player.getName());
		bet[index][0] = bool;
	}
	
	public void setPreviousBet(Player player, Integer newBet)
	{
		int index = this.getPlayerIndex(player.getName());
		previousBets[index][0] = newBet;
	}
	
	public void setRestriction(Player player, Boolean what, String which)
	{
		int index = getPlayerIndex(player.getName());
		switch(which)
		{
			case "ca":
				rescritions[index][0] = what;
				break;
			case "ch":
				rescritions[index][2] = what;
				break;
			case "ra":
				rescritions[index][1] = what;
				break;
			case "fo":
				rescritions[index][3] = what;
				break;
			case "al":
				rescritions[index][4] = what;
				break;
		}
	}
	
	private int getPlayerIndex(String name)
	{
		for(int i = 0; i < this.players.size(); i++)
			if(players.get(i).getName().equals(name))
				return i;
		return -1;
	}
	
	public void addTo(Player newInfo)
	{
		String name = newInfo.getName();
		Player temp;
		String compareName;
		int indexToChange = -1;
		for(int i = 0; i < players.size(); i++)
		{
			temp = players.get(i);
			compareName = temp.getName();
			if(compareName.equals(name))
			{
				indexToChange = i;
				break;
			}
		}
		if(indexToChange == -1)
			return;
		else
			players.set(indexToChange, newInfo);
	}
	
	public void removePlayer(Player toRemove)
	{
		String name = toRemove.getName();
		Player temp;
		String compareName;
		for(int i = 0; i < players.size(); i++)
		{
			temp = players.get(i);
			compareName = temp.getName();
			if(compareName.equals(name))
			{
				SerializableFilesManager.savePlayer(players.get(i));
				players.remove(i);
				return;
			}
		}
	}
	
	public ServerSocket getMyServer()
	{return this.serv;}
	public ArrayList<LobbyThread> getPlayerThreads()
	{return this.playerThreads;}
	public ArrayList<Player> getPlayers()
	{return this.players;}
	public int getPort()
	{return this.myPort;}
	
}
