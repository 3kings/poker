package server.update;

import java.net.*;

public class UpdateServer extends Thread
{
	private ServerSocket serv;
	
	public UpdateServer()
	{
		super("UpdateServer");
	}
	
	public void run()
	{
		Socket client;
		try
		{
			serv = new ServerSocket(10004);
			while(true)
			{
				client = serv.accept();
				new UpdateThread(client).start();
			}
		}
		catch(Exception e)
		{e.printStackTrace();}
	}

}
