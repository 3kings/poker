package server.update;

import java.io.*;
import java.net.Socket;

public class UpdateThread extends Thread
{
	BufferedInputStream input; //not used
	BufferedInputStream fileInput;
	BufferedOutputStream output;
	
	public UpdateThread(Socket client) throws IOException
	{
		super("UpdateThread");
		output = new BufferedOutputStream(client.getOutputStream());
		input = new BufferedInputStream(client.getInputStream());
	}
	
	/**
	 * Sends the connected client the most up to date jar file
	 */
	public void run()
	{
		try
		{
			File perm = new File(System.getProperty("user.dir")+"/GameClient.jar");
			fileInput = new BufferedInputStream(new FileInputStream(perm));
			
			byte[] buffer = new byte[1024];
			int numRead;
			while((numRead = fileInput.read(buffer)) != -1)
				output.write(buffer, 0, numRead);
			
			fileInput.close();
			output.flush();
			output.close();
			input.close();
			this.interrupt();
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
}
