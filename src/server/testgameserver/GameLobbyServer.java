package server.testgameserver;

import global.dgframework.GameAuthDatagram;
import global.dgframework.InviteDatagram;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import server.mainserver.MainServer;



//Jason Gardella
//Jun 6, 2013
//Description:
//Block D

public class GameLobbyServer extends Thread
{
	
	private MainServer parentServer;
	private ServerSocket servSocket;
	private Socket socket;
	private ObjectInputStream ois;
	private ArrayList<Game> games = new ArrayList<Game>();
	
	public GameLobbyServer(MainServer mainServer)
	{
		super("TestGameServer");
		parentServer = mainServer;
	}

	
	public void run()
	{
		try {
			servSocket = new ServerSocket(6000);

		} catch (IOException e1) {
			e1.printStackTrace();
		}
		while(true)
		{
			try {
				new GameLobbyServerListenThread(this, servSocket.accept()).start();
			} catch (IOException e) 
			{e.printStackTrace();}
		}
	}
	
	public int addToFirstOpen(Game game)
	{
		for(int i = 0; i < games.size(); i++)
			if(games.get(i) == null)
			{
				games.set(i, game);
				return i;
			}
		games.add(game);
		return games.size() - 1;
	}
	
	public void sendStartGameSignals(int gid, String gameType)
	{
		System.out.println("TestGameServer sending start game signals!");
		for(Game game : games)
			if(game.getGID() == gid)
				game.sendStartGameSignals(gameType);
	}
	
	public ArrayList<Game> getGameList()
	{
		return games;
	}

	public void sendInvite(InviteDatagram gram)
	{
		parentServer.sendInvite(gram);
	}

}
