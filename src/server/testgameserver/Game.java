package server.testgameserver;

import global.Player;
import global.dgframework.BanKickDatagram;
import global.dgframework.Chat;
import global.dgframework.ChatUpdateDatagram;
import global.dgframework.Datagram;
import global.dgframework.DatagramHandler;
import global.dgframework.InviteDatagram;
import global.dgframework.UserUpdateDatagram;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

//Jason Gardella
//Jun 6, 2013
//Description:
//Block D

public class Game
{
	
	private int gid;
	private String chat;
	private GameLobbyServer server;
	private ArrayList<Player> users;
	private Player user;
	private ArrayList<GameLobbyServerListenThread> clients;
	
	public Game(Socket socket, GameLobbyServer server)
	{
		this.server = server;
		clients = new ArrayList<GameLobbyServerListenThread>();
		users = new ArrayList<Player>();
	}
	
	public void sendStartGameSignals(String gameType)
	{
		System.out.println("Game sending start game signals!");
		for(GameLobbyServerListenThread thread: clients)
			thread.sendStartGameSignal(gameType);
	}
	
	public void setGID(int gid)
	{
		this.gid = gid;
	}
	
	public int getGID()
	{
		return gid;
	}

	public void setAdminUser(Player user) 
	{
		this.user = user;
	}
	
	public void updateChats(String s)
	{
		System.out.println(clients.size());
		for(GameLobbyServerListenThread thread : clients)
		{
			System.out.println("Sending chat update.");
			thread.sendChatUpdate(s);
		}
	}

	public void sendInvite(InviteDatagram gram)
	{
		gram.setGID(gid);
		server.sendInvite(gram);
	}
	
	public void addToClients(GameLobbyServerListenThread thread)
	{
		clients.add(thread);
	}
	
	public void updateUsers(Object obj)
	{
		users.add((Player) obj);
		sendUserUpdates();
	}
	
	public void sendUserUpdates()
	{
		for(GameLobbyServerListenThread thread : clients)
			thread.sendUserUpdate(listToArray(users));
	}
	
	public void removeUser(Player user)
	{
		for(int i = 0; i < users.size(); i++)
			if(users.get(i).equals(user))
				users.remove(i);
		if(users.size() != 0)
			sendUserUpdates();
		else
			server.getGameList().remove(gid);
	}
	
	private Player[] listToArray(ArrayList<Player> list)
	{
		Player[] array = new Player[list.size()];
		for(int i = 0; i < list.size(); i++)
			array[i] = list.get(i);
		return array;
	}

	public void newAdmin()
	{
		if(users.size() != 1)
			for(GameLobbyServerListenThread thread : clients)
				if(!thread.isAdmin())
				{
					thread.sendAdminUpdate();
					user = thread.getUser();
					for(int i = 0; i < users.size(); i++)
						if(users.get(i).equals(user))
							users.set(i, new Player(user.getName(), user.getIP(), true));
					return;
				}
	}

	public void kickUser(BanKickDatagram gram)
	{
		for(GameLobbyServerListenThread thread : clients)
			if(thread.getUser().getName().equals(gram.getUser()))
				thread.sendKick(gram);
	}

}
