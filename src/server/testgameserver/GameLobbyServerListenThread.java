package server.testgameserver;

import global.Player;
import global.dgframework.AdminUpdateDatagram;
import global.dgframework.BanKickDatagram;
import global.dgframework.Chat;
import global.dgframework.ChatUpdateDatagram;
import global.dgframework.Datagram;
import global.dgframework.DatagramHandler;
import global.dgframework.GameAuthDatagram;
import global.dgframework.GameStartDatagram;
import global.dgframework.InviteDatagram;
import global.dgframework.UserHolder;
import global.dgframework.UserUpdateDatagram;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class GameLobbyServerListenThread extends Thread implements Chat, UserHolder
{
	
	private GameLobbyServer parent;
	private Socket socket;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private int gid;
	private boolean on;
	private Player user;
	private boolean isAdmin;
	
	public GameLobbyServerListenThread(GameLobbyServer parent, Socket socket)
	{
		this.parent = parent;
		this.socket = socket;
		on = true;
		try {
			ois = new ObjectInputStream(socket.getInputStream());
			oos = new ObjectOutputStream(socket.getOutputStream());
			GameAuthDatagram gram = (GameAuthDatagram) ois.readObject();
			user = gram.getUser();
			isAdmin = gram.isAdmin();
			if(gram.isAdmin())
			{
				int gid = parent.addToFirstOpen(new Game(socket, parent));
				this.gid = gid;
				parent.getGameList().get(gid).setGID(gid);
				parent.getGameList().get(gid).setAdminUser(gram.getUser());
				parent.getGameList().get(gid).addToClients(this);
			}
			else
			{
				parent.getGameList().get(gram.getGID()).addToClients(this);
				this.gid = gram.getGID();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println(gid);
	}
	
	public void run()
	{
		while(on)
		{
			try
			{
				DatagramHandler.parseDatagram((Datagram)ois.readObject(), this);
			} catch (ClassNotFoundException e)
			{e.printStackTrace();}
			catch (IOException e)
			{on = false;}
		}
		if(isAdmin)
			parent.getGameList().get(gid).newAdmin();
		parent.getGameList().get(gid).removeUser(user);
		System.out.println("GameServerListenThread ending.");
	}

	public void sendChatUpdate(String s)
	{
		try
		{
			oos.writeObject(new ChatUpdateDatagram(s));
		} catch (IOException e)
		{e.printStackTrace();}
	}
	
	public void updateChat(String s)
	{
		parent.getGameList().get(gid).updateChats(s);
	}
	
	public void sendInvite(InviteDatagram gram)
	{
		gram.setGID(gid);
		parent.sendInvite(gram);
	}

	@Override
	public String getChat()
	{
		return null;
	}
	
	public boolean isAdmin()
	{
		return isAdmin;
	}
	
	public void initiateGameStart(String game)
	{
		System.out.println("GameStart initiated!");
		parent.sendStartGameSignals(gid, game);
	}
	
	public void sendStartGameSignal(String gameType)
	{
		System.out.println("Game server listen thread sending start game signal!");
		try
		{
			oos.writeObject(new GameStartDatagram(gameType));
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void updateUsers(Object obj)
	{
		System.out.println("GameServerListenThread updating users.");
		parent.getGameList().get(gid).updateUsers(obj);
	}

	public void sendUserUpdate(Player[] users)
	{
		try
		{
			oos.writeObject(new UserUpdateDatagram(users));
		} catch (IOException e)
		{on = false;}
	}

	public void sendAdminUpdate()
	{
		try
		{
			oos.writeObject(new AdminUpdateDatagram(true));
		} catch (IOException e)
		{e.printStackTrace();}
		isAdmin = true;
	}

	public void kickUser(BanKickDatagram gram)
	{
		parent.getGameList().get(gid).kickUser(gram);
	}
	
	public Player getUser()
	{
		return user;
	}

	public void sendKick(BanKickDatagram gram)
	{
		try
		{
			oos.writeObject(gram);
		} catch (IOException e)
		{e.printStackTrace();}
	}

}
