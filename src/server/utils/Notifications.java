package server.utils;

import java.io.*;
import java.net.*;

import global.dgframework.NotificationDatagram;

public class Notifications extends Thread
{
	private String note;
	private String ip;
	
	public Notifications(String note, String ip)
	{
		this.note = note;
		this.ip = ip;
	}
	
	public void run()
	{
		try
		{
			Socket soc = new Socket(ip,15000);
			ObjectOutputStream output = new ObjectOutputStream(soc.getOutputStream());
			ObjectInputStream input = new ObjectInputStream(soc.getInputStream());
			output.writeObject(new NotificationDatagram(note));
			output.flush();
			
			output.close();
			input.close();
			soc.close();
		}
		catch(Exception e)
		{e.printStackTrace();}
	}

}
