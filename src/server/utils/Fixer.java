package server.utils;

import global.Player;
import global.SerializableFilesManager;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import server.encrypt.Encrypter;

public class Fixer
{
	
	public static void encryptAllPasswords()
	{
		Player loaded;
		ArrayList<File> files = getAllPlayersFiles();
		String password;
		for(int i = 0; i < files.size(); i++)
		{
			loaded = SerializableFilesManager.loadPlayer(files.get(i).getName().replaceAll(".p", ""));
			password = Encrypter.encypt(loaded.getPassword());
			loaded.setPassword(password);
			SerializableFilesManager.savePlayer(loaded);
		}
	}
	
	public static void printAllMessages(String userName)
	{
		Player loaded;
		loaded = SerializableFilesManager.loadPlayer(userName);
		loaded.printMessages();
	}
	
	public static void printAllPlayersMessages()
	{
		ArrayList<File> files = getAllPlayersFiles();
		for(int i = 0; i < files.size(); i++)
			SerializableFilesManager.loadPlayer(files.get(i).getName().replaceAll(".p", "")).printMessages();
	}
	
	public static void addMessage(String username, ArrayList<String> message)
	{
		Player loaded;
		loaded = SerializableFilesManager.loadPlayer(username);
		loaded.addMessage(message);
		SerializableFilesManager.savePlayer(loaded);
	}
	
	public static void clearAllMessages(String username)
	{
		Player loaded;
		loaded = SerializableFilesManager.loadPlayer(username);
		loaded.clearMessages();
		SerializableFilesManager.savePlayer(loaded);
	}
	
	public static void clearAllPlayersMessages()
	{
		ArrayList<File> files = getAllPlayersFiles();
		Player loaded;
		for(int i = 0; i < files.size(); i++)
		{
			loaded = SerializableFilesManager.loadPlayer(files.get(i).getName().replaceAll(".p", ""));
			loaded.clearMessages();
			SerializableFilesManager.savePlayer(loaded);
		}
	}
	
	public static void backUpAllPlayers()
	{
		ArrayList<File> files = getAllPlayersFiles();
		for(int i = 0; i < files.size(); i++)
			SerializableFilesManager.createBackup(files.get(i).getName().replaceAll(".p", ""));
	}
	
	public static void loadAndSave()
	{
		ArrayList<File> files = getAllPlayersFiles();
		for(int i = 0; i < files.size(); i++)
			SerializableFilesManager.savePlayer(SerializableFilesManager.loadPlayer(files.get(i).getName().replaceAll(".p", "")));
	}
	
	public static void unbanAllPlayers()
	{
		Player loaded;
		ArrayList<File> files = getAllPlayersFiles();
		for(int i = 0; i < files.size(); i++)
		{
			loaded = SerializableFilesManager.loadPlayer(files.get(i).getName().replaceAll(".p", ""));
			loaded.setBanned(false);
			SerializableFilesManager.savePlayer(loaded);
		}
	}
	
	public static void clearAllHands()
	{
		Player loaded;
		ArrayList<File> files = getAllPlayersFiles();
		for(int i = 0; i < files.size(); i++)
		{
			loaded = SerializableFilesManager.loadPlayer(files.get(i).getName().replaceAll(".p", ""));
			loaded.clearHand();
			SerializableFilesManager.savePlayer(loaded);
		}
	}
	
	public static void banAllPlayers()
	{
		Player loaded;
		ArrayList<File> files = getAllPlayersFiles();
		for(int i = 0; i < files.size(); i++)
		{
			loaded = SerializableFilesManager.loadPlayer(files.get(i).getName().replaceAll(".p", ""));
			loaded.setBanned(true);
			SerializableFilesManager.savePlayer(loaded);
		}
	}
	
	public static void setAllPlayersOffline()
	{
		Player loaded;
		ArrayList<File> files = getAllPlayersFiles();
		for(int i = 0; i < files.size(); i++)
		{
			loaded = SerializableFilesManager.loadPlayer(files.get(i).getName().replaceAll(".p", ""));
			loaded.setOnline(false);
			SerializableFilesManager.savePlayer(loaded);
		}
	}
	
	public static void zeroAllChips()
	{
		Player loaded;
		ArrayList<File> files = getAllPlayersFiles();
		for(int i = 0; i < files.size(); i++)
		{
			loaded = SerializableFilesManager.loadPlayer(files.get(i).getName().replaceAll(".p", ""));
			loaded.setChips(0);
			SerializableFilesManager.savePlayer(loaded);
		}
	}
	
	private static ArrayList<File> getAllPlayersFiles()
	{
		ArrayList<File> allFiles = new ArrayList<File>();
		File[] list;
		File path = new File("data/players/");
		list = path.listFiles();
		for(int i = 0; i < list.length; i++)
			allFiles.add(list[i]);
		return allFiles;
	}
	
	public static ArrayList<String> getAllPlayerNames()
	{
		ArrayList<String> allNames = new ArrayList<String>();
		File[] list;
		File path = new File("data/players/");
		list = path.listFiles();
		for(int i = 0; i < list.length; i++)
			allNames.add(list[i].getName().replaceAll(".p", ""));
		return allNames;
	}
	
	public static void setBanStatus(String username, boolean bool)
	{
		Player player;
		player = SerializableFilesManager.loadPlayer(username);
		player.setBanned(bool);
		SerializableFilesManager.savePlayer(player);
	}
	
	public static void setOnlineStatus(String username, boolean bool)
	{
		Player player;
		player = SerializableFilesManager.loadPlayer(username);
		player.setOnline(bool);
		SerializableFilesManager.savePlayer(player);
	}
	
	public static void setName(String oldName, String newName)
	{
		Player player;
		player = SerializableFilesManager.loadPlayer(oldName);
		player.setName(newName);
		SerializableFilesManager.savePlayer(player);
	}
	
	public static void setChips(String name, Integer chips)
	{
		Player player;
		player = SerializableFilesManager.loadPlayer(name);
		player.setChips(chips.intValue());
		SerializableFilesManager.savePlayer(player);
	}
	
	public static void setAdmin(String name, boolean bol)
	{
		Player player;
		player = SerializableFilesManager.loadPlayer(name);
		player.setAdmin(bol);
		SerializableFilesManager.savePlayer(player);
	}
	
	public static void setHand(String name, ArrayList<ImageIcon> newCards)
	{
		Player player;
		player = SerializableFilesManager.loadPlayer(name);
		player.setHand(newCards);
		SerializableFilesManager.savePlayer(player);
	}
	
	public static void setPassword(String name, String newPass)
	{
		Player player;
		player = SerializableFilesManager.loadPlayer(name);
		player.setPassword(newPass);
		SerializableFilesManager.savePlayer(player);
	}
	
	public static void setIp(String name, String newIp)
	{
		Player player;
		player = SerializableFilesManager.loadPlayer(name);
		player.setIP(newIp);
		SerializableFilesManager.savePlayer(player);
	}
	
	public static Player loadPlayer(String name)
	{
		return SerializableFilesManager.loadPlayer(name);
	}

	public static void main(String[] args)
	{
		//Player a = Fixer.loadPlayer("admin");
		//System.out.println("Password == null: "+(a.isOnline()));
		//a.setOnline(false);
		//SerializableFilesManager.savePlayer(a);
	}

}
