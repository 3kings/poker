package server.lobbyserver;

import global.Player;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

import server.mainserver.MainServer;


//Jason Gardella
//May 22, 2013
//Description:
//Block D

public class LobbyPanel extends JPanel implements ActionListener
{
	
	private MainServer mainServer;
	private JTextArea lobbyChat, userList;
	private JScrollPane chatSP, userListSP;
	private JLabel chatLabel, userListLabel;
	private JButton startStop;
	
	public LobbyPanel(MainServer server)
	{
		mainServer = server;
		lobbyChat = new JTextArea();
		lobbyChat.setEditable(false);
		chatSP = new JScrollPane(lobbyChat);
		chatSP.setPreferredSize(new Dimension(280, 55));
		((DefaultCaret) lobbyChat.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		chatLabel = new JLabel("Chat");
		add(chatLabel);
		add(chatSP);
		
		userList = new JTextArea();
		userList.setEditable(false);
		userListSP = new JScrollPane(userList);
		userListSP.setPreferredSize(new Dimension(280, 55));
		((DefaultCaret) userList.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		userListLabel = new JLabel("Users");
		add(userListLabel);
		add(userListSP);
		
		startStop = new JButton("Start");
		startStop.setActionCommand("startStop");
		startStop.addActionListener(this);
		startStop.setBackground(Color.green);
		
		add(startStop);
	}
	
	public void updateChat(String s)
	{
		lobbyChat.append(s + "\n");
	}
	
	public String getChat()
	{
		return lobbyChat.getText();
	}

	public void updateUserList(ArrayList<Player> users) 
	{
		userList.setText("");
		for(Player user: users)
			userList.append(user.getName() + "   "+user.getIP()+"\n");
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		if(e.getActionCommand().equals("startStop"))
			if(startStop.getText().equals("Start"))
			{
				mainServer.toggleLobbyServer(true);
				startStop.setBackground(Color.red);
				startStop.setText("Stop");
			}
			else
				if(startStop.getText().equals("Stop"))
				{
					mainServer.toggleLobbyServer(false);
					startStop.setBackground(Color.green);
					startStop.setText("Start");
				}
			
	}
	
}
