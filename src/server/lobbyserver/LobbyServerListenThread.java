package server.lobbyserver;

import global.Player;
import global.dgframework.Chat;
import global.dgframework.ChatUpdateDatagram;
import global.dgframework.Datagram;
import global.dgframework.DatagramHandler;
import global.dgframework.UserHolder;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class LobbyServerListenThread extends Thread implements Chat, UserHolder
{
	
	private LobbyServerThread parentThread;
	private ObjectInputStream ois;
	private boolean on = true;

	
	public LobbyServerListenThread(LobbyServerThread parentThread) throws IOException
	{
		super("LobbyServerListenThread");
		this.parentThread = parentThread;
		ois = new ObjectInputStream(parentThread.getSocket().getInputStream());
	}
	
	public void run()
	{
		while(on)
		{
			try
			{
				DatagramHandler.parseDatagram((Datagram)ois.readObject(), this);
			} catch (Exception e)
			{
				on = false;
				if(ois != null)
					parentThread.setStat(false);
			}
		}
	}

	@Override
	public void updateChat(String s)
	{
		parentThread.updateChat(s);
	}

	@Override
	public String getChat()
	{
		return parentThread.getChat();
	}

	public void updateUsers(Object obj) 
	{
		parentThread.updateUsers((Player)obj);
	}
	
	public void setStatus(boolean bool) throws IOException
	{
		on = bool;
		if(ois == null && parentThread != null && on)
		{
			ois = new ObjectInputStream(parentThread.getSocket().getInputStream());
		}
		else
		{
			if(!on && ois != null)
			{
				ois.close();
				ois = null;
			}
		}
	}
	
	public void slashCommands(String s)
	{
		try
		{
			if(parentThread.getUser().isAdmin())
			{
				ArrayList<String> list = new ArrayList<String>();
				if(!(s.contains("/ban") || s.contains("/kick") || s.contains("/unban")))
					return;
				list.add(s.substring(spaceNum(s, 1) + 2, spaceNum(s, 2))); // Command
				list.add(s.substring(spaceNum(s, 2) + 1, spaceNum(s, 3))); // User
				list.add(s.substring(spaceNum(s, 3) + 1, s.length())); // Reason
				parentThread.slashCommands(list);
			}
		}
		catch(Exception e)
		{return;}
	}
	
	private int spaceNum(String s, int numOfSpaces)
	{
		int spaceNum = 0;
		for(int i = 0; i < s.length(); i++)
		{
			if(s.charAt(i) == ' ')
				spaceNum++;
			if(spaceNum == numOfSpaces)
				return i;
		}
		return -1;
	}

	
}
