package server.lobbyserver;

import global.Player;
import global.Player;
import global.SerializableFilesManager;
import global.dgframework.BanKickDatagram;
import global.dgframework.Chat;
import global.dgframework.Datagram;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class LobbyServerThread extends Thread implements Chat
{
	private Socket socket;
	private LobbyServer parentServer;
	private ObjectOutputStream oos;
	private boolean on = true;
	private Player user;
	public static String chatPool = "";
	private LobbyServerListenThread listenThread;
	
	public LobbyServerThread(Socket socket, LobbyServer parentServer) throws IOException
	{
		super("LobbyServerThread");
		this.socket = socket;
		this.parentServer = parentServer;
		
		oos = new ObjectOutputStream(this.socket.getOutputStream());
		listenThread = new LobbyServerListenThread(this);
		listenThread.start();
	}
	
	@Override
	public void updateChat(String s)
	{
		if(chatPool.equals(""))
			chatPool = chatPool + s;
		else
			chatPool = chatPool + "\n" + s;
		parentServer.updateChat(s);
	}

	@Override
	public String getChat()
	{
		return chatPool;
	}
	
	public Socket getSocket()
	{
		return socket;
	}
	
	public void setStat(boolean bool)
	{
		try
		{
			on = bool;
			listenThread.setStatus(bool);
			setUserOffline(user);
			parentServer.removeUser(user);
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	public void setStatus(boolean bool)
	{
		try
		{
			on = bool;
			listenThread.setStatus(bool);
			if(!on)
			{
				setUserOffline(user);
			}
		}
		catch(Exception e)
		{setUserOffline(user);e.printStackTrace();}
	}
	
	public Player getUser()
	{
		return user;
	}
	
	public void updateUsers(Player user)
	{
		this.user = user;
		setUserOnline(user);
		parentServer.updateUserList(user);
	}
	
	private void setUserOnline(Player user)
	{
		if(user != null)
		{
			user.setOnline(true);
			SerializableFilesManager.savePlayer(user);
		}
		else
		{
			System.out.println("Could not set player online");
		}
	}
	
	private void setUserOffline(Player user)
	{
		if(user != null)
		{
			user.setOnline(false);
			SerializableFilesManager.savePlayer(user);
		}
		else
		{
			System.out.println("Could not set player online");
		}
	}
	
	public void sendDatagram(Datagram gram)
	{
		try
		{
			oos.writeObject(gram);
		} catch (IOException e)
		{e.printStackTrace();}
	}

	public void banUser(BanKickDatagram gram)
	{
		try
		{
			oos.writeObject(gram);
			on = false;
		} catch (IOException e)
		{
			System.err.println("Error in LobbyServerThread.java, Could not write object to lobby client");
		}
	}
	
	public void slashCommands(ArrayList<String> list)
	{
		if(isAdmin())
			parentServer.slashCommands(list);
	}
	
	private boolean isAdmin()
	{
		if(user != null)
			return user.isAdmin();
		return false;
	}

}
