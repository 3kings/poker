package server.lobbyserver;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Locale;
import java.util.SimpleTimeZone;

public class LobbyServerChatLogger extends Thread
{
	
	private LobbyServer parentServer;
	private PrintWriter out;
	
	public LobbyServerChatLogger(LobbyServer parentServer)
	{
		super("LobbyServerChatLogger");
		this.parentServer = parentServer;
	}
	
	public void run()
	{
		while(true)
		{
			Calendar currentTime = Calendar.getInstance(new SimpleTimeZone(-6, "EST"), Locale.US);
			if(currentTime.get(Calendar.HOUR_OF_DAY) == 0 && currentTime.get(Calendar.MINUTE) == 0 
				 && currentTime.get(Calendar.SECOND) == 0 && currentTime.get(Calendar.MILLISECOND) == 0)
			{
				try
				{
					out = new PrintWriter(currentTime.get(Calendar.DAY_OF_MONTH) + "/" + currentTime.get(Calendar.MONTH) + 
							"/" + currentTime.get(Calendar.YEAR)+".txt");
					out.write(LobbyServerThread.chatPool);
					LobbyServerThread.chatPool = "";
				} catch (FileNotFoundException e)
				{
					System.err.println("Error in LobbyServerChatLogger.java, Could not open file.");
					e.printStackTrace();
				}
			}
		}
	}
	
}
