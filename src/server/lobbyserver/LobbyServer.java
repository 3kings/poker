package server.lobbyserver;

import global.Player;
import global.SerializableFilesManager;
import global.dgframework.BanKickDatagram;
import global.dgframework.ChatUpdateDatagram;
import global.dgframework.InviteDatagram;
import global.dgframework.UserUpdateDatagram;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import server.mainserver.MainServer;

public class LobbyServer extends Thread
{
	private ServerSocket servSocket;
	private MainServer server;
	private boolean on;
	private ArrayList<LobbyServerThread> threads;
	private ArrayList<Player> users;
	
	public LobbyServer(MainServer server) throws IOException
	{
		super("LobbyServer");
		servSocket = new ServerSocket(5002);
		servSocket.close();
		new LobbyServerChatLogger(this).start();
		this.server = server;
		on = false;
		threads = new ArrayList<LobbyServerThread>();
		users = new ArrayList<Player>();
	}
	
	public void distributeInvites(InviteDatagram gram)
	{
		if(gram.getUsers() != null)
		{
			ArrayList<String> users = gram.getUsers();
			for(LobbyServerThread thread : threads)
				for(String user : users)
					if(thread.getUser().getName().equals(user))
						thread.sendDatagram(gram);
		}
		else
		{
			threads.get((int)( Math.random() * threads.size())).sendDatagram(gram);
		}
	}
	
	public ArrayList<LobbyServerThread> getAllUsers()
	{
		return this.threads;
	}
	
	public void run()
	{
		Socket socket;
		while(true)
		{
			if(on)
			{
				try
				{
					socket = servSocket.accept();
					threads.add(new LobbyServerThread(socket, this));
					threads.get(threads.size() - 1).start();
				} catch (IOException e )
				{
					System.err.println("Error in LobbyServer.java, Something with threads");
				}
			}
			else
			{
				try
				{
					Thread.sleep(10);
				}
				catch(Exception e)
				{e.printStackTrace();}
			}
		}
	}
	
	public void stopAllThreads()
	{
		for(int i = 0; i < threads.size(); i++)
		{
			if(threads.size() == 0)
				break;
			threads.get(i).setStatus(false);
			threads.remove(i);
			users.remove(i);
			i--;
		}
		server.updateUserList(users);
		threads.clear();
		users.clear();
	}
	
	public void updateChat(String s)
	{
		server.updateLobbyChat(s);
		for(LobbyServerThread thread : threads)
			thread.sendDatagram(new ChatUpdateDatagram(s));
	}

	public void updateUserList(Player user) 
	{
		users.add(user);
		server.updateUserList(users);
		sendUserListUpdates();
	}
	
	public void sendUserListUpdates()
	{
		for(LobbyServerThread thread : threads)
			thread.sendDatagram(new UserUpdateDatagram(listToArray(addAdminIndicator(users))));
	}
	
	public void setStatus(boolean status)
	{
		on = status;
		try
		{
			if(!on)
			{
				stopAllThreads();
				servSocket.close();
			}
			else
				if(servSocket.isClosed())
				{
					servSocket = new ServerSocket(5002);
					servSocket.setSoTimeout(0);
				}
		} catch (IOException e)
		{
			System.err.println("Error in LobbyServer.java, Could not bind address or stop all threads.");
			e.printStackTrace();
		}
	}

	public void banUser(String user, BanKickDatagram gram)
	{
		for(int i = 0; i < threads.size(); i++)
		{
			if(threads.get(i).getUser().getName().equals(user))
			{
				threads.get(i).banUser(gram);
				threads.remove(i);
				users.remove(i);
				i--;
			}
		}
		this.sendUserListUpdates();
		server.updateUserList(users);
	}
	
	public void slashCommands(ArrayList<String> list)
	{
		server.slashCommands(list);
	}
	
	private Player[] listToArray(ArrayList<Player> list)
	{
		Player[] array = new Player[list.size()];
		for(int i = 0; i < list.size(); i++)
			array[i] = list.get(i);
		return array;
	}

	public void removeUser(Player user)
	{
		for(int i = 0; i < threads.size(); i++)
		{
			if(threads.get(i).getUser().equals(user))
			{
				threads.remove(i);
				users.remove(i);
				for(LobbyServerThread thread : threads)
					thread.sendDatagram(new UserUpdateDatagram(listToArray(users)));
				server.updateUserList(users);
				return;
			}
		}
	}
	
	private ArrayList<Player> addAdminIndicator(ArrayList<Player> list)
	{
		ArrayList<Player> newList = new ArrayList<Player>();
		for(Player user : list)
		{
			if(SerializableFilesManager.containsPlayer(user.getName()))
			{
				if(SerializableFilesManager.loadPlayer(user.getName()).isAdmin())
				{
					Player p = SerializableFilesManager.loadPlayer(user.getName());
					newList.add(new Player(user.getName() + "*", p.getChips(), user.getIP(), p.getPassword(), true, p.getAvatar()));
				}
				else
				{
					Player p = SerializableFilesManager.loadPlayer(user.getName());
					newList.add(new Player(user.getName(), p.getChips(), user.getIP(), p.getPassword(), false, p.getAvatar()));
				}
			}
		}
		return newList;
	}
	
	public ArrayList<Player> getUsers()
	{return users;}
	
}
