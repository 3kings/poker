package server.mainserver;

import global.Player;
import global.SerializableFilesManager;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class MakeAdminFrame extends JFrame implements ActionListener
{
	private JTextField textField;
	private MainServer server;
	
	public MakeAdminFrame(MainServer server) 
	{
		this.server = server;
		
		getContentPane().setLayout(null);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setBounds(49, 42, 89, 23);
		getContentPane().add(btnSubmit);
		btnSubmit.addActionListener(this);
		
		textField = new JTextField();
		textField.setBounds(72, 11, 86, 20);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblUser = new JLabel("User");
		lblUser.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblUser.setBounds(39, 14, 46, 14);
		getContentPane().add(lblUser);
		
		setResizable(false);
		setPreferredSize(new Dimension(221, 103));
		pack();
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		Player player;
		if(SerializableFilesManager.containsPlayer(textField.getText()))
		{
			player = SerializableFilesManager.loadPlayer(textField.getText());
			if(player != null)
				player.setAdmin(true);
			SerializableFilesManager.savePlayer(player);
		}
		server.updateLobbyUserLists();
		dispose();
	}
	
}
