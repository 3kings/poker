package server.mainserver;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.JButton;

public class UnbanUserFrame extends JFrame implements ActionListener
{
	private JTextField textField;
	public UnbanUserFrame() 
	{
		if(!new File("blacklist.txt").exists())
		{
			JOptionPane.showMessageDialog(this, "No users have been banned.", "No Banned Users", JOptionPane.INFORMATION_MESSAGE);
			dispose();
		}
		else
		{
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setTitle("Unban User");
			setAlwaysOnTop(true);
			getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			
			JLabel lblUsername = new JLabel("Username");
			lblUsername.setFont(new Font("Tahoma", Font.BOLD, 12));
			getContentPane().add(lblUsername);
			
			textField = new JTextField();
			getContentPane().add(textField);
			textField.setColumns(10);
			
			JButton btnUnban = new JButton("Unban");
			getContentPane().add(btnUnban);
			btnUnban.addActionListener(this);
			
			setPreferredSize(new Dimension(180, 98));
			pack();
			setVisible(true);
		}
	}
	@Override
	public void actionPerformed(ActionEvent e)
	{
		try
		{
			File temp = new File("tmp.txt");
			File blacklist = new File("blacklist.txt");
			BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
			BufferedReader br = new BufferedReader(new FileReader(blacklist));
			int lineNum = 0, lineNumTemp = 0;
			String line = br.readLine();
			String newFile = "";
			while(line != null)
			{
				lineNum++;
				if(!line.equals(textField.getText()))
				{
					newFile += line + "\n";
					lineNumTemp++;
				}
				line = br.readLine();
			}
			br.close();
			if(lineNumTemp == lineNum)
				JOptionPane.showMessageDialog(this, "User not found in blacklist.", "User Not Found", JOptionPane.INFORMATION_MESSAGE);
			else
			{
				bw.write(newFile);
				bw.close();
				if(blacklist.delete());
					temp.renameTo(blacklist);
				this.dispose();
			}
		} catch (IOException e1)
		{
			e1.printStackTrace();
		}
	}
	
	

}
