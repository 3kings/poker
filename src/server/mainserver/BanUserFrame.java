package server.mainserver;

import global.dgframework.BanKickDatagram;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.FlowLayout;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class BanUserFrame extends JFrame implements ActionListener
{
	private JTextField userField;
	private MainServer server;
	private JTextField reasonField;
	private JComboBox comboBox;
	
	public BanUserFrame(MainServer server) 
	{
		setAlwaysOnTop(true);
		setTitle("Ban/Kick User");
		this.server = server;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		
		JLabel lblUser = new JLabel("User");
		getContentPane().add(lblUser);
		
		userField = new JTextField();
		userField.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(userField);
		userField.setColumns(10);
		
		JLabel lblReason = new JLabel("Reason");
		getContentPane().add(lblReason);
		
		reasonField = new JTextField();
		getContentPane().add(reasonField);
		reasonField.setColumns(10);
		
		JButton btnEnter = new JButton("Enter");
		btnEnter.addActionListener(this);
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Kick", "Ban"}));
		comboBox.setSelectedIndex(0);
		getContentPane().add(comboBox);
		getContentPane().add(btnEnter);
		setPreferredSize(new Dimension(100,200));
		pack();
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		if(comboBox.getSelectedIndex() == 0)
			server.banKickUser(userField.getText(), new BanKickDatagram(reasonField.getText(), false));
		else
			if(comboBox.getSelectedIndex() == 1)
				server.banKickUser(userField.getText(), new BanKickDatagram(reasonField.getText(), true));
		dispose();
	}
	
}
