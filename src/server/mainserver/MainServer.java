package server.mainserver;

import global.Player;
import global.SerializableFilesManager;
import global.dgframework.BanKickDatagram;
import global.dgframework.InviteDatagram;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import server.information.InfoServer;
import server.lobbyserver.LobbyPanel;
import server.lobbyserver.LobbyServer;
import server.testgameserver.GameLobbyServer;
import server.update.UpdateServer;
import server.userpasserver.UserPassMultServer;
import server.utils.Fixer;


public class MainServer implements ActionListener
{
	private JFrame frame = new JFrame("Server!");
	private JTabbedPane serversPanel = new JTabbedPane();
	private JPanel contentPanel = new JPanel();
	private JPanel mainPanel = new JPanel();
	private LobbyPanel lobbyPanel = new LobbyPanel(this);
	private JPanel userPanel = new JPanel();
	private JPanel regPanel = new JPanel();
	private JPanel chipPanel = new JPanel();
	private JButton banKickUser = new JButton("Ban/Kick User");
	private JButton unbanUser = new JButton("Unban User");
	private JButton makeAdmin = new JButton("Make Admin");
	private final Dimension tabbedPaneSize = new Dimension(300, 275);
	
	private InfoServer infoServer;
	
	private LobbyServer lobbyServer = new LobbyServer(this);
	private UserPassMultServer userPassServer = new UserPassMultServer();
	
	private UpdateServer updateServer = new UpdateServer();
	
	// Using this for testing the invite system.
	private GameLobbyServer gameServer = new GameLobbyServer(this);
	private server.gameserver.blackjack.MainServer blackJackServer = new server.gameserver.blackjack.MainServer();
	
	public static boolean usingJar = false;
	
	//builds the Main Server Frame
	public void setFrame()
	{
		frame.setDefaultCloseOperation(setOffline());
		frame.setPreferredSize(new Dimension(315, 400));
		serversPanel.setPreferredSize(tabbedPaneSize);
		serversPanel.addTab("User", userPanel);
		serversPanel.addTab("Lobby", lobbyPanel);
		serversPanel.setBackgroundAt(1, Color.red);
		serversPanel.addTab("Reg", regPanel);
		serversPanel.addTab("Chips", chipPanel);
		
		banKickUser.setActionCommand("ban");
		banKickUser.addActionListener(this);
		
		unbanUser.setActionCommand("unban");
		unbanUser.addActionListener(this);
		
		makeAdmin.setActionCommand("admin");
		makeAdmin.addActionListener(this);
		
		userPanel.setPreferredSize(tabbedPaneSize);
		lobbyPanel.setPreferredSize(tabbedPaneSize);
		regPanel.setPreferredSize(tabbedPaneSize);
		chipPanel.setPreferredSize(tabbedPaneSize);
		
		mainPanel.setPreferredSize(new Dimension(300, 125));
		mainPanel.add(banKickUser);
		mainPanel.add(unbanUser);
		mainPanel.add(makeAdmin);
		
		contentPanel.setPreferredSize(new Dimension(315, 375));
		contentPanel.add(serversPanel);
		contentPanel.add(mainPanel);
		

		frame.getContentPane().add(contentPanel);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	//method for closing the main server is will first backUpAllPlayers then clear their hands and then set them offline before it shuts off
	private int setOffline()
	{
		//create a save all players method accesing every game server/the lobby server to save all of them (tasks)
		Fixer.backUpAllPlayers();
		Fixer.clearAllHands();
		Fixer.setAllPlayersOffline();
		
		return JFrame.EXIT_ON_CLOSE;
	}
	
	//when server is started this method will backup all players and clear all hands
	private void setOnline()
	{
		Fixer.backUpAllPlayers();
		Fixer.clearAllHands();
	}
	//updates the lobby chat panel with the most recent chat messages
	public void updateLobbyChat(String s)
	{
		lobbyPanel.updateChat(s);
	}
	
	//starts every server along with MainServer
	public MainServer(boolean usingJar) throws IOException
	{
		infoServer = new InfoServer(this);
		infoServer.start();
		updateServer.start();
		lobbyServer.start();
		userPassServer.start();
		gameServer.start();
		blackJackServer.start();
		
		MainServer.usingJar = usingJar;
		
		setOnline();
		setFrame();
	}

	public static void main(String[] args) throws Exception
	{
		if(args.length != 0)
			new MainServer(Boolean.parseBoolean(args[0]));
		else
			new MainServer(usingJar);
			
	}
	//updates the user list on the main server's lobby panel
	public void updateUserList(ArrayList<Player> users) 
	{
		lobbyPanel.updateUserList(users);
		this.updateLobbyUserLists();
	}
	//this method is called when someone presses the button on the lobby panel
	public void toggleLobbyServer(boolean bool)
	{
		lobbyServer.setStatus(bool);
		if(bool)
			serversPanel.setBackgroundAt(1, Color.green);
		else
			serversPanel.setBackgroundAt(1, Color.red);
	}
	
	//action listeners for the buttons on the main panel
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if(e.getActionCommand().equals("ban"))
		{
			new BanUserFrame(this);
		}
		else
		{
			if(e.getActionCommand().equals("unban"))
			{
				new UnbanUserFrame();
			}
			else
			{
				if(e.getActionCommand().equals("admin"))
				{
					new MakeAdminFrame(this);
				}
			}
		}
	}
	
	//unbans a specific user
	private void unbanUser(String user)
	{
		Player player;
		if((player = SerializableFilesManager.loadPlayer(user)) != null)
		{
			player.setBanned(false);
			SerializableFilesManager.savePlayer(player);
		}
		else
		{
			System.out.println("Player could not be found");
		}
	}
	//sends a datagram down to lobby server to tell that user's specific client that he/she is banned or kicked form the main lobby
	public void banKickUser(String user, BanKickDatagram gram)
	{
		lobbyServer.banUser(user, gram);
	}
	//this is just for easy access to ban or kick someone
	public void slashCommands(ArrayList<String> list)
	{
		if(list.get(0).equals("ban"))
			lobbyServer.banUser(list.get(1), new BanKickDatagram(list.get(2), true));
		else
			if(list.get(0).equals("kick"))
				lobbyServer.banUser(list.get(1), new BanKickDatagram(list.get(2), false));
			else
				if(list.get(0).equals("unban"))
					unbanUser(list.get(1));
					
	}
	//distributes invites throughout the list of players
	public void sendInvite(InviteDatagram gram)
	{
		lobbyServer.distributeInvites(gram);
	}
	//calls lobby server's method sendUserListUpdates which in turns will send all the players in the lobby
	public void updateLobbyUserLists()
	{
		lobbyServer.sendUserListUpdates();
	}
	
	public LobbyServer getLobbyServer()
	{return this.lobbyServer;}
	public GameLobbyServer getGameLobbyServer()
	{return this.gameServer;}
	public server.gameserver.blackjack.MainServer getBlackJackServer()
	{return this.blackJackServer;}

}
