package global;

import java.awt.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import javax.swing.*;
import server.mainserver.MainServer;
import client.login.UserPass;

public class Cards
{
	private ImageIcon backCard = new ImageIcon("cards/backCard.jpg");
	private boolean client = true;
	private String back = "/cards/backCard.jpg";
	private String[] cardFolders = {
			"clubs","diamonds","hearts","spades"
	};
	private String[] clubs = {
			"aceClubs.jpg","eightClubs.jpg","fiveClubs.jpg","fourClubs.jpg","jackClubs.jpg",
			"kingClubs.jpg","nineClubs.jpg","queenClubs.jpg","sevenClubs.jpg","sixClubs.jpg",
			"tenClubs.jpg","threeClubs.jpg","twoClubs.jpg"
	};
	private String[] diamonds = {
			"aceDia.jpg","eightDia.jpg","fiveDia.jpg","fourDia.jpg","jackDia.jpg","kingDia.jpg",
			"nineDia.jpg","queenDia.jpg","sevenDia.jpg","sixDia.jpg","tenDia.jpg","threeDia.jpg",
			"twoDia.jpg"
	};
	private String[] hearts = {
			"aceHearts.jpg","eightHearts.jpg","fiveHearts.jpg","fourHearts.jpg","jackHearts.jpg",
			"kingHearts.jpg","nineHearts.jpg","queenHearts.jpg","sevenHearts.jpg","sixHearts.jpg",
			"tenHearts.jpg","threeHearts.jpg","twoHearts.jpg"
	};
	private String[] spades = {
			"aceSpades.jpg","eightSpades.jpg","fiveSpades.jpg","fourSpades.jpg","jackSpades.jpg",
			"kingSpades.jpg","nineSpades.jpg","queenSpades.jpg","sevenSpades.jpg","sixSpades.jpg",
			"tenSpades.jpg","threeSpades.jpg","twoSpades.jpg"
	};
	private String[][] all = {
			clubs,diamonds,hearts,spades
	};
	private ArrayList<ImageIcon> allCards = new ArrayList<ImageIcon>();
	
	public Cards(boolean isUsingClient)
	{
		this.client = isUsingClient;
		try
		{
			addCardsAndChangeSize();
			shuffle();
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	/**
	 * @param x Cards name with extension
	 * @return Face Value of Card
	 */
	public static int getFaceValue(String x)
	{
		int face = 0;
		switch(x)
		{
			case "aceClubs.jpg":
			case "aceDia.jpg":
			case "aceHearts.jpg":
			case "aceSpades.jpg":
				face = 1;
				break;
			case "eightClubs.jpg":
			case "eightDia.jpg":
			case "eightHearts.jpg":
			case "eightSpades.jpg":
				face = 8;
				break;
			case "fiveClubs.jpg":
			case "fiveDia.jpg":
			case "fiveHearts.jpg":
			case "fiveSpades.jpg":
				face = 5;
				break;
			case "fourClubs.jpg":
			case "fourDia.jpg":
			case "fourHearts.jpg":
			case "fourSpades.jpg":
				face = 4;
				break;
			case "jackClubs.jpg":
			case "jackDia.jpg":
			case "jackHearts.jpg":
			case "jackSpades.jpg":
			case "kingClubs.jpg":
			case "kingDia.jpg":
			case "kingHearts.jpg":
			case "kingSpades.jpg":
			case "queenClubs.jpg":
			case "queenDia.jpg":
			case "queenHearts.jpg":
			case "queenSpades.jpg":
			case "tenClubs.jpg":
			case "tenDia.jpg":
			case "tenHearts.jpg":
			case "tenSpades.jpg":
				face = 10;
				break;
			case "twoClubs.jpg":
			case "twoDia.jpg":
			case "twoHearts.jpg":
			case "twoSpades.jpg":
				face = 2;
				break;
			case "threeClubs.jpg":
			case "threeDia.jpg":
			case "threeHearts.jpg":
			case "threeSpades.jpg":
				face = 3;
				break;
			case "sixClubs.jpg":
			case "sixDia.jpg":
			case "sixHearts.jpg":
			case "sixSpades.jpg":
				face = 6;
				break;
			case "sevenClubs.jpg":
			case "sevenDia.jpg":
			case "sevenHearts.jpg":
			case "sevenSpades.jpg":
				face = 7;
				break;
			case "nineClubs.jpg":
			case "nineDia.jpg":
			case "nineHearts.jpg":
			case "nineSpades.jpg":
				face = 9;
				break;
		}
		return face;
	}
	
	//shuffles all the cards in the deck
	private void shuffle()
	{
		long seed = System.nanoTime();
		Collections.shuffle(allCards, new Random(seed));
	}
	
	/**
	 * Chooses a card at random from the deck. Also removes that card when chosen
	 * @return randomly chosen card
	 */
	public ImageIcon getRandomCard()
	{
		int index = ((int)Math.random() * allCards.size());
		return allCards.remove(index);
	}
	
	/**
	 * @return Image of the back of a card
	 */
	public ImageIcon getBackCard()
	{return backCard;}
	
	public static ImageIcon parseImage(String x)
	{
		if(x.contains("Dia"))
			return new ImageIcon("cards/diamonds/"+x);
		else
			if(x.contains("Clubs"))
				return new ImageIcon("cards/clubs/"+x);
			else
				if(x.contains("Hearts"))
					return new ImageIcon("cards/hearts/"+x);
				else
					if(x.contains("Spades"))
						return new ImageIcon("cards/spades/"+x);
		return null;
	}
	
	//adds all the cards and adds a description to them loaded into memory
	private void addCardsAndChangeSize() throws Exception
	{
		String tempString;
		ImageIcon tempIcon;
		allCards.clear();
		URL imageURL;
		
		if(client)
		{
			if(UserPass.usingJar)
			{
				for(int i = 0; i < all.length; i++)
				{
					for(int x = 0; x < clubs.length; x++)
					{
						tempString = all[i][x];
						tempString = "/cards/"+cardFolders[i]+"/"+tempString;
						imageURL = this.getClass().getResource(tempString);
						tempIcon = resizeImage(new ImageIcon(imageURL),70,70,false);
						tempString = all[i][x];
						tempIcon.setDescription(tempString);
						allCards.add(tempIcon);
					}
				}
				backCard = resizeImage(new ImageIcon(this.getClass().getResource(back)),70,70,false);
			}
			else
			{
				for(int i = 0; i < all.length; i++)
				{
					for(int x = 0; x < clubs.length; x++)
					{
						tempString = all[i][x];
						tempString = "cards/"+cardFolders[i]+"/"+tempString;
						tempIcon = resizeImage(new ImageIcon(tempString),70,70,false);
						tempString = all[i][x];
						tempIcon.setDescription(tempString);
						allCards.add(tempIcon);
					}
				}
				backCard = resizeImage(backCard,70,70,false);
			}
		}
		else
		{
			if(MainServer.usingJar)
			{
				for(int i = 0; i < all.length; i++)
				{
					for(int x = 0; x < clubs.length; x++)
					{
						tempString = all[i][x];
						tempString = "/cards/"+cardFolders[i]+"/"+tempString;
						imageURL = this.getClass().getResource(tempString);
						tempIcon = resizeImage(new ImageIcon(imageURL),70,70,false);
						tempString = all[i][x];
						tempIcon.setDescription(tempString);
						allCards.add(tempIcon);
					}
				}
				backCard = resizeImage(new ImageIcon(this.getClass().getResource(back)),70,70,false);
			}
			else
			{
				for(int i = 0; i < all.length; i++)
				{
					for(int x = 0; x < clubs.length; x++)
					{
						tempString = all[i][x];
						tempString = "cards/"+cardFolders[i]+"/"+tempString;
						tempIcon = resizeImage(new ImageIcon(tempString),70,70,false);
						tempString = all[i][x];
						tempIcon.setDescription(tempString);
						allCards.add(tempIcon);
					}
				}
				backCard = resizeImage(backCard,70,70,false);
			}
		}
	}
	
	//resizes images
	public ImageIcon resizeImage(ImageIcon imageIcon, int width, int height, boolean max) 
	{
		Image image = imageIcon.getImage();
		Image newimg = image.getScaledInstance(-1, height, java.awt.Image.SCALE_SMOOTH);
		int width1 = newimg.getWidth(null);
		if ((max && width1 > width) || (!max && width1 < width))
		    newimg = image.getScaledInstance(width, -1, java.awt.Image.SCALE_SMOOTH);
		return new ImageIcon(newimg);
	}
}
