/**
 *
 */
package global.dgframework;

public class GameStartDatagram extends Datagram
{
	
	private String game;
	
	public GameStartDatagram(String game)
	{
		this.game = game;
	}
	
	public String getGame()
	{
		return game;
	}
	
}
