package global.dgframework;

public class BanKickDatagram extends Datagram
{
	
	private String reason, user;
	private boolean ban;
	
	public BanKickDatagram(String reason, boolean ban)
	{
		this.reason = reason;
		this.ban = ban;
	}
	
	public BanKickDatagram(String user, String reason)
	{
		this.reason = reason;
		this.user = user;
	}
	
	public String getReason()
	{ return reason; }
	
	public String getUser()
	{ return user; }
	
	public boolean isBan()
	{ return ban; }

}
