package global.dgframework;

public class VerifyDatagram extends Datagram
{
	int update;
	byte[] hash;
	
	public VerifyDatagram(int update, byte[] hash)
	{
		this.update = update;
		this.hash = hash;
	}
	public VerifyDatagram(byte[] hash)
	{
		this.hash = hash;
	}
	public VerifyDatagram(int update)
	{
		this.update = update;
	}
	
	public int getUpdate()
	{return this.update;}
	public byte[] getHash()
	{return this.hash;}
}
