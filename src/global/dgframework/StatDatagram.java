package global.dgframework;

import java.util.ArrayList;

import javax.swing.ImageIcon;

import global.Player;

public class StatDatagram extends Datagram
{
	private Player player;
	private String name;
	
	public StatDatagram(String name)
	{
		this.name = name;
	}
	public StatDatagram(Player player)
	{
		this.player = player;
	}
	
	public Player getPlayer()
	{return this.player;}
	public String getName()
	{return this.name;}
	
}
