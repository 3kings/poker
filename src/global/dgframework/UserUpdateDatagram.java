package global.dgframework;

import global.Player;


public class UserUpdateDatagram extends Datagram
{
	
	private Player user;
	private Player[] users;
	
	public UserUpdateDatagram(Player lobbyServerUser)
	{
		this.user = lobbyServerUser;
	}
	
	public UserUpdateDatagram(Player[] users)
	{
		this.users = users;
	}

	public Object getContents()
	{
		return (user != null) ? user : (users != null) ? users : null;
	}
	
}
