package global.dgframework;

public class AdminUpdateDatagram extends Datagram
{
	
	private boolean admin;
	
	public AdminUpdateDatagram(boolean admin)
	{
		this.admin = admin;
	}
	
	public boolean getContents()
	{
		return admin;
	}
	
}
