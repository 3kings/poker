package global.dgframework;

import java.util.ArrayList;

public class InviteDatagram extends Datagram
{
	
	private ArrayList<String> users;
	private String adminUser;
	private String game;
	private int gid;
	
	public InviteDatagram(ArrayList<String> users, String adminUser, String game, int gid)
	{
		this.users = users;
		this.adminUser = adminUser;
		this.game = game;
		this.gid = gid;
	}
	
	public void setGID(int gid)
	{
		this.gid = gid;
	}
	
	public ArrayList<String> getUsers()
	{
		return users;
	}
	
	public String getSendingUser()
	{
		return adminUser;
	}
	
	public String getGame()
	{
		return game;
	}
	
	public int getGID()
	{
		return gid;
	}
	
}
