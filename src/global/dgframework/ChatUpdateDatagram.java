package global.dgframework;


public class ChatUpdateDatagram extends Datagram
{
	
	private String updateText;
	
	public ChatUpdateDatagram(String s)
	{
		updateText = s;
	}
	
	public String getContents()
	{
		return updateText;
	}
	
}
