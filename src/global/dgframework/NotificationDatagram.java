package global.dgframework;

public class NotificationDatagram extends Datagram
{
	private String message;
	
	public NotificationDatagram(String message)
	{
		this.message = message;
	}
	
	public String getContents()
	{
		return message;
	}
}
