package global.dgframework;

public class MessageDatagram extends Datagram
{
	private String users;
	private String from;
	private String subject;
	private String message;
	
	public MessageDatagram(String users, String from, String subject, String message)
	{
		this.users = users;
		this.from = from;
		this.subject = subject;
		this.message = message;
	}
	
	public String getUsers()
	{return this.users;}
	public String getFrom()
	{return this.from;}
	public String getSubject()
	{return this.subject;}
	public String getMessage()
	{return this.message;}
}
