package global.dgframework;

import java.util.ArrayList;

public class PlayersDatagram extends Datagram
{
	private ArrayList<String> names;
	
	public PlayersDatagram(ArrayList<String> names)
	{
		this.names = names;
	}
	
	public ArrayList<String> getNames()
	{return this.names;}
}
