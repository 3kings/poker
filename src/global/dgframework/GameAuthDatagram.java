package global.dgframework;

import global.Player;

public class GameAuthDatagram extends Datagram
{
	
	private boolean isAdmin;
	private Player user;
	private int gid;
	
	
	public GameAuthDatagram(boolean isAdmin, Player user2, int gid)
	{
		this.isAdmin = isAdmin;
		this.user = user2;
		this.gid = gid;
	}
	
	public boolean isAdmin()
	{
		return isAdmin;
	}
	
	public Player getUser()
	{
		return user;
	}
	
	public int getGID()
	{
		return gid;
	}
	
}
