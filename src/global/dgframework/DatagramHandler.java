package global.dgframework;

import server.information.InfoThread;
import server.lobbyserver.LobbyServerListenThread;
import server.testgameserver.Game;
import server.testgameserver.GameLobbyServerListenThread;
import server.userpasserver.UserPassServer;
import client.gamelobby.GameLobby;
import client.gamelobby.GameLobbyListenThread;
import client.login.Login;
import client.mainlobby.LobbyClientThread;
import client.message.HandleNotification;

public class DatagramHandler
{
	
	public static void parseDatagram(Datagram gram, Object parentComponent)
	{
		if(gram instanceof ChatUpdateDatagram)
		{
			if(parentComponent instanceof Chat)
			{
				if(!((ChatUpdateDatagram) gram).getContents().contains("/"))
					((Chat) parentComponent).updateChat(((ChatUpdateDatagram) gram).getContents());
				else
					((LobbyServerListenThread) parentComponent).slashCommands(((ChatUpdateDatagram) gram).getContents());
			}
		}
		else
		{
			if(gram instanceof UserUpdateDatagram)
			{
				((UserHolder)parentComponent).updateUsers(((UserUpdateDatagram) gram).getContents());
			}
			else
			{
				if(gram instanceof BanKickDatagram)
				{
					if(parentComponent instanceof LobbyClientThread)
						((LobbyClientThread)parentComponent).userBanned(((BanKickDatagram) gram).getReason(), ((BanKickDatagram) gram).isBan());
					else
						if(parentComponent instanceof GameLobbyServerListenThread)
							((GameLobbyServerListenThread)parentComponent).kickUser((BanKickDatagram)gram);
						else
							if(parentComponent instanceof GameLobbyListenThread)
								((GameLobbyListenThread)parentComponent).kickUser((BanKickDatagram)gram);
				}
				else
				{
					if(gram instanceof InviteDatagram)
					{
						if(parentComponent instanceof LobbyClientThread)
						{
							((LobbyClientThread)parentComponent).displayInvite(((InviteDatagram) gram).getSendingUser(), ((InviteDatagram) gram).getGame(), ((InviteDatagram) gram).getGID());
						}
						else
						{
							if(parentComponent instanceof GameLobbyServerListenThread)
							{
								((GameLobbyServerListenThread)parentComponent).sendInvite((InviteDatagram) gram);
							}
						}
					}
					else
					{
						if(gram instanceof AdminUpdateDatagram)
						{
							((GameLobbyListenThread)parentComponent).setAdmin(((AdminUpdateDatagram)gram).getContents());
						}
						else
						{
							if(gram instanceof GameStartDatagram)
							{
								System.out.println("Recieved a gram that is GameStartDatagram");
								if(parentComponent instanceof GameLobbyListenThread)
								{
									((GameLobbyListenThread)parentComponent).startGame(((GameStartDatagram)gram).getGame());
								}
								else
								{
									if(parentComponent instanceof GameLobbyServerListenThread)
									{
										((GameLobbyServerListenThread)parentComponent).initiateGameStart(((GameStartDatagram)gram).getGame());
									}
								}
							}
							else
							{
								if(gram instanceof NotificationDatagram)
								{
									((HandleNotification)parentComponent).showNotification(((NotificationDatagram)gram).getContents());
								}
								else
								{
									if(gram instanceof StatDatagram)
									{
										if(parentComponent instanceof InfoThread)
										{
											((InfoThread)parentComponent).sendStats(((StatDatagram)gram).getName());
										}
									}
									else
									{
										if(gram instanceof VerifyDatagram)
										{
											if(parentComponent instanceof InfoThread)
											{
												((InfoThread)parentComponent).sendVert(new String(((VerifyDatagram)gram).getHash()));
											}
										}
										else
										{
											if(gram instanceof PlayersDatagram)
											{
												if(parentComponent instanceof InfoThread)
												{
													((InfoThread)parentComponent).sendPlayers();
												}
											}
											else
											{
												if(gram instanceof CredenDatagram)
												{
													if(parentComponent instanceof UserPassServer)
													{
														CredenDatagram info = (CredenDatagram)gram;
														if(info.isWhat())
														{
															((UserPassServer)parentComponent).write(info.getUser(), info.getPass(), info.getIp(), info.getAvatar());
														}
														else
														{
															((UserPassServer)parentComponent).checkCre(info.getUser(), info.getPass());
														}
													}
												}
												else
												{
													if(gram instanceof MessageDatagram)
													{
														if(parentComponent instanceof InfoThread)
														{
															((InfoThread)parentComponent).sendToAllPlayers((MessageDatagram)gram);
														}
													}
													else
													{
														//if gram instance of
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				
			}
		}
				
	}

}
