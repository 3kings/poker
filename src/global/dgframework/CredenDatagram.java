package global.dgframework;

import javax.swing.ImageIcon;

public class CredenDatagram extends Datagram
{
	private String user,pass,ip;
	private ImageIcon avatar;
	private boolean what;
	boolean reg,ban,online;
	
	public CredenDatagram(String user, String pass, String ip, boolean what)
	{
		this.user = user;
		this.pass = pass;
		this.ip = ip;
		this.what = what;
		this.avatar = null;
	}
	
	public CredenDatagram(boolean reg)
	{
		this.reg = reg;
	}
	
	public CredenDatagram(boolean what, boolean reg, boolean ban, boolean online)
	{
		this.what = what;
		this.reg = reg;
		this.ban = ban;
		this.online = online;
	}
	
	public CredenDatagram(String user, String pass, String ip, ImageIcon avatar, boolean what)
	{
		this.user = user;
		this.pass = pass;
		this.ip = ip;
		this.avatar = avatar;
		this.what = what;
	}
	
	public String getUser()
	{return this.user;}
	public String getPass()
	{return this.pass;}
	public String getIp()
	{return this.ip;}
	public ImageIcon getAvatar()
	{return this.avatar;}
	public boolean isWhat()
	{return this.what;}
	public boolean isReg()
	{return this.reg;}
	public boolean isBan()
	{return this.ban;}
	public boolean isOnline()
	{return this.online;}
}
