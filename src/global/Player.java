package global;

import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.ImageIcon;

//Jason Gardella
//May 21, 2013
//Description:
//Block D

public class Player implements Serializable
{
	//do not remove this it will cause all players data to be lost
	private static final long serialVersionUID = -1719376365462537981L;
	
	private String name;
	private int chipCount;
	private String myIP;
	private ImageIcon avatar;
	private ArrayList<ImageIcon> hand = new ArrayList<ImageIcon>();
	private String password;
	private boolean banned = false;
	private boolean online = false;
	private boolean isAdmin = false;
	private boolean isMod = false;
	
	private Integer[] wins = new Integer[5]; //0 = blackjack, 1 = texas hold em, 2,3,4,5...... other games
	private Integer[] loses = new Integer[5]; //0 = blackjack, 1 = texas hold em, 2,3,4,5...... other games
	
	private ArrayList<ArrayList<String>> messages = new ArrayList<ArrayList<String>>(); //going to be implementing a message system
	private String signature; //users signature
	private ArrayList<ArrayList<String>> savedGroups = new ArrayList<ArrayList<String>>(); //saved user groups
	private Integer messageQue = new Integer(0);
	
	public Player(String name, String ip)
	{
		this.name = name;
		this.myIP = ip;
	}
	public Player(String name, String ip, boolean isAdmin)
	{
		this.name = name;
		this.myIP = ip;
		this.isAdmin = isAdmin;
	}
	public Player(String name, int chipCount, String ip)
	{
		this.name = name;
		this.chipCount = chipCount;
		myIP = ip;
	}
	public Player(String name, int chipCount, String ip, ImageIcon avatar)
	{
		this.name = name;
		this.chipCount = chipCount;
		this.myIP = ip;
		this.avatar = avatar;
	}
	public Player(String name, int chipCount, String ip, String password, ImageIcon avatar)
	{
		this.name = name;
		this.chipCount = chipCount;
		this.myIP = ip;
		this.avatar = avatar;
		this.password = password;
	}
	public Player(String name, int chipCount, String ip, String password, boolean isAdmin)
	{
		this.name = name;
		this.chipCount = chipCount;
		this.myIP = ip;
		this.password = password;
		this.isAdmin = isAdmin;
	}
	public Player(String name, int chipCount, String ip, boolean isAdmin, ImageIcon avatar)
	{
		this.name = name;
		this.chipCount = chipCount;
		this.myIP = ip;
		this.isAdmin = isAdmin;
		this.avatar = avatar;
	}
	public Player(String name, int chipCount, String ip, String password, boolean isAdmin, ImageIcon avatar)
	{
		this.name = name;
		this.chipCount = chipCount;
		this.myIP = ip;
		this.password = password;
		this.isAdmin = isAdmin;
		this.avatar = avatar;
	}
	public Player(String name, int chipCount, String ip, String password)
	{
		this.name = name;
		this.chipCount = chipCount;
		this.myIP = ip;
		this.password = password;
	}
	
	public boolean equals(Object obj)
	{
		if(this.getName().equals(((Player)obj).getName()))
			return true;
		return false;
	}
	
	public ArrayList<ImageIcon> getHand()
	{return hand;}
	public void addToHand(ImageIcon card)
	{hand.add(card);}
	public void clearHand()
	{hand.clear();}
	public void removeFromHand(int index)
	{hand.remove(index);}
	
	public Integer[] getWins()
	{return this.wins;}
	public Integer[] getLoses()
	{return this.loses;}
	
	public void setBlackJackWins(Integer i)
	{this.wins[0] = i;}
	public void setTexasWins(Integer i)
	{this.wins[1] = i;}
	
	public void setBlackJackLoses(Integer i)
	{this.loses[0] = i;}
	public void setTexasLoses(Integer i)
	{this.loses[1] = i;}
	
	public void incrementBlackJackWins()
	{this.wins[0]++;}
	public void incrementBlackJackLoses()
	{this.loses[0]++;}
	
	public void incrementTexasWins()
	{this.wins[1]++;}
	public void incrementTexasLoses()
	{this.loses[1]++;}
	
	public boolean isBanned()
	{return banned;}
	public void setBanned(boolean bool)
	{banned = bool;}
	public boolean isOnline()
	{return online;}
	public boolean isAdmin()
	{return isAdmin;}
	public void setOnline(boolean bool)
	{online = bool;}
	public String getPassword()
	{return password;}
	public void setPassword(String password)
	{this.password = password;}
	public void setAvatar(ImageIcon newAva)
	{this.avatar = newAva;}
	
	public ImageIcon getAvatar()
	{return this.avatar;}
	public String getIP()
	{return myIP;}
	public String getName()
	{return name;}
	public int getChips()
	{return chipCount;}
	public void setChips(int chips)
	{chipCount = chips;}
	public void setName(String userName)
	{name = userName;}
	public void setIP(String IP)
	{myIP = IP;}
	public void setAdmin(boolean bool)
	{this.isAdmin = bool;}
	public void setMod(boolean bool)
	{this.isMod = bool;}
	public boolean isMod()
	{return isMod;}
	public void setHand(ArrayList<ImageIcon> hand)
	{this.hand = hand;}
	
	public void printMessages()
	{
		if(messages == null || messages.size() == 0)
		{
			System.out.println("No messages for "+this.getName());
			return;
		}
		for(int i = 0; i < messages.size(); i++)
			System.out.println(messages.get(i));
	}
	public void constructMessages()
	{messages = new ArrayList<ArrayList<String>>();}
	public void addMessage(ArrayList<String> message)
	{if(messages != null) messages.add(0,message);}
	public void clearMessages()
	{if(messages != null) messages.clear();}
	public ArrayList<ArrayList<String>> getMessages()
	{return this.messages;}
	
	public void setMessageQue(Integer a)
	{this.messageQue = a;}
	public Integer getMessageQue()
	{return this.messageQue;}
	public void incrementMessageQue()
	{if(this.messageQue == null) messageQue = new Integer(0); this.messageQue++;}
	
	public String toString()
	{
		return getName()+" "+getChips()+" "+getIP()+" "+getHand();
	}

}
