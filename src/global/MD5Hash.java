package global;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;

public class MD5Hash 
{
	public static final String algo = "MD5";
	
	public static String hash(File filename) throws Exception 
	{  
		byte[] b = createCheckSum(filename);  
		String result = "";  
		for (int i=0; i < b.length; i++)   
			result +=  
					Integer.toString(( b[i] & 0xff )+ 0x100, 16).substring(1);  
		return result;  
	 }
	
	public static byte[] createCheckSum(File filename) throws Exception
	{  
		BufferedInputStream fis = new BufferedInputStream(new FileInputStream(filename));  
		byte[] buffer = new byte[1024];  
		MessageDigest complete = MessageDigest.getInstance(algo);
		int numRead;  
		do 
		{  
			if ((numRead = fis.read(buffer)) > 0)   
				complete.update(buffer, 0, numRead);   
		} 
		while (numRead != -1);  
		fis.close();  
		return complete.digest();  
	 }
	
	public static boolean verify(String hash1, String hash2)
	{
		return hash1.equalsIgnoreCase(hash2);
	}
}
