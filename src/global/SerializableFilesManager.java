package global;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.channels.FileChannel;

public class SerializableFilesManager
{
	private static final String PATH = "data/players/";
	private static final String BACKUP_PATH = "data/playersBackup/";
	
	public synchronized static final boolean containsPlayer(String username) 
	{return new File(PATH + username + ".p").exists();}
	
	public synchronized static Player loadPlayer(String username) 
	{
        try 
        {
        	return (Player) loadSerializedFile(new File(PATH+username+".p"));
        } 
        catch (Throwable e) 
        {System.out.println(e);}
        try 
        {
        	System.out.println("Recovering account: "+ username);
            return (Player)loadSerializedFile(new File(BACKUP_PATH+username+".p"));
        }
        catch (Throwable e) 
        {System.out.println(e);}
        return null;
	}
	
	public static boolean createBackup(String username) 
	{
        try 
        {
        	copyFile(new File(PATH +username+".p"), new File(BACKUP_PATH+username+".p"));
            return true;
        } 
        catch(Throwable e) 
        {System.out.println(e);return false;}
	 }
	        
    public static void copyFile(File sourceFile, File destFile) throws IOException 
    {
        if (!destFile.exists()) 
        	destFile.createNewFile();

        FileChannel source = null;
        FileChannel destination = null;
        try 
        {
        	source = new FileInputStream(sourceFile).getChannel();
        	destination = new FileOutputStream(destFile).getChannel();
        	destination.transferFrom(source, 0, source.size());
        } 
        finally 
        {
        	if (source != null)
        		source.close();
            if (destination != null)
            	destination.close();
        }
    }
	
    public synchronized static void savePlayer(Player player) 
    {
        try 
        {
             storeSerializableClass(player, new File(PATH+player.getName()+".p"));
        } 
        catch (Exception e) 
        {e.printStackTrace();}
    }
	
	public static final Object loadSerializedFile(File f) throws IOException,ClassNotFoundException 
	{
        if (!f.exists())
                return null;
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(f));
        Object object = in.readObject();
        in.close();
        return object;
	}
	
	public static final void storeSerializableClass(Serializable o, File f) throws IOException 
	{
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(f));
        out.writeObject(o);
        out.close();
	}
	
	private SerializableFilesManager() 
	{}
	
	public static void main(String[] args)
	{
	}
}