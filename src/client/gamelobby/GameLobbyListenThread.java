package client.gamelobby;

import global.dgframework.BanKickDatagram;
import global.dgframework.Chat;
import global.dgframework.Datagram;
import global.dgframework.DatagramHandler;
import global.dgframework.UserHolder;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

import javax.swing.JOptionPane;


public class GameLobbyListenThread extends Thread implements Chat, UserHolder
{
	
	private GameLobby parent;
	private ObjectInputStream ois;
	private boolean on;
	
	public GameLobbyListenThread(GameLobby lobby, Socket socket)
	{
		super("GameLobbyListenThread");
		parent = lobby;
		try {
			ois = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		on = true;
	}
	
	public void run()
	{
		while(on)
		{
			try {
				DatagramHandler.parseDatagram((Datagram)ois.readObject(), this);
			} catch (ClassNotFoundException e) 
			{e.printStackTrace();}
			catch (IOException e) 
			{on = false;}
		}
	}

	@Override
	public void updateChat(String s) 
	{
		parent.updateChat(s);
	}

	@Override
	public String getChat()
	{
		return parent.getChat();
	}

	@Override
	public void updateUsers(Object obj)
	{
		parent.updateUsers(obj);
	}

	public void setAdmin(boolean admin)
	{
		parent.setAdmin(admin);
	}

	public void startGame(String gameType)
	{
		parent.startGame(gameType);
	}

	public void kickUser(BanKickDatagram gram)
	{
		JOptionPane.showMessageDialog(parent, "You have been kicked for the reason: "+gram.getReason(),
				"User Kicked", JOptionPane.WARNING_MESSAGE, null);
		on = false;
		try
		{
			ois.close();
		} catch (IOException e)
		{e.printStackTrace();}
		parent.dispose();
	}
	
}
