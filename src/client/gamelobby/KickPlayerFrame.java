package client.gamelobby;

import global.dgframework.BanKickDatagram;

import javax.swing.JFrame;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JButton;

public class KickPlayerFrame extends JFrame implements ActionListener
{
	private JTextField userField, reasonField;
	private GameLobby parent;
	
	public KickPlayerFrame(GameLobby parent) 
	{
		
		this.parent = parent;
		
		getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblPlayer = new JLabel("Player");
		lblPlayer.setFont(new Font("Tahoma", Font.BOLD, 12));
		getContentPane().add(lblPlayer);
		
		userField = new JTextField();
		getContentPane().add(userField);
		userField.setColumns(10);
		
		JLabel lblReason = new JLabel("Reason");
		lblReason.setFont(new Font("Tahoma", Font.BOLD, 12));
		getContentPane().add(lblReason);
		
		reasonField = new JTextField();
		getContentPane().add(reasonField);
		reasonField.setColumns(10);
		
		JButton btnKick = new JButton("Kick");
		btnKick.addActionListener(this);
		getContentPane().add(btnKick);
		
		setPreferredSize(new Dimension(100, 150));
		pack();
		setVisible(true);
		setResizable(false);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		parent.sendDatagram(new BanKickDatagram(userField.getText(), reasonField.getText()));
		dispose();
	}
	
	
	
}
