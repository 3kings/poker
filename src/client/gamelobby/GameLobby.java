package client.gamelobby;

import global.Player;
import global.dgframework.Chat;
import global.dgframework.ChatUpdateDatagram;
import global.dgframework.Datagram;
import global.dgframework.GameAuthDatagram;
import global.dgframework.GameStartDatagram;
import global.dgframework.UserUpdateDatagram;

import javax.swing.JFrame;
import javax.swing.JPanel;

import javax.swing.JTextArea;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.border.LineBorder;
import javax.swing.text.DefaultCaret;

import client.gameclient.blackjack.BlackClientOut;
import client.login.UserPass;
import client.mainlobby.LobbyClient;

public class GameLobby extends JFrame implements ActionListener, Chat
{
	
	private JTextField chatInput;
	private JTextArea chatArea, playerListArea;
	private String game;
	private Player adminUser;
	private Socket socket;
	private ObjectOutputStream oos;
	private boolean isAdmin;
	private int reqPlayers;
	private Player[] users;
	private int gid;
	private JButton btnStartGame, btnInvitePlayer, btnKickPlayer;
	private JLabel gameAdminLabel;
	private JLabel gipLabel;
	
	public GameLobby(String game, boolean isAdmin, int gid, Player lobbyServerUser) 
	{
		setResizable(false);
		getContentPane().setBackground(new Color(0, 100, 0));
		setBackground(Color.LIGHT_GRAY);
		
		System.out.println(gid);
		this.gid = gid;
		this.game = game;
		this.isAdmin = isAdmin;
		
		if(game.equals("Blackjack"))
		{
			reqPlayers = 4;
		}
		getContentPane().setLayout(null);
		
		JPanel options = new JPanel();
		options.setBounds(10, 216, 414, 35);
		getContentPane().add(options);
		
		btnInvitePlayer = new JButton("Invite Player(s)");
		btnInvitePlayer.setActionCommand("invite");
		btnInvitePlayer.addActionListener(this);
		
		btnStartGame = new JButton("Start Game");
		btnStartGame.setEnabled(false);
		btnStartGame.setActionCommand("start");
		btnStartGame.addActionListener(this);

		options.add(btnStartGame);
		options.add(btnInvitePlayer);
		
		btnKickPlayer = new JButton("Kick Player");
		btnKickPlayer.setActionCommand("kick");
		btnKickPlayer.addActionListener(this);
		options.add(btnKickPlayer);
		
		if(isAdmin)
		{
			btnStartGame.setToolTipText(game+" requires  "+reqPlayers+" players.");
			this.adminUser = UserPass.PLAYER;
			gameAdminLabel = new JLabel("GAME ADMIN");
		}
		else
		{
			this.adminUser = lobbyServerUser;
			btnInvitePlayer.setToolTipText("Only the admin can invite players.");
			btnStartGame.setToolTipText("Only the admin can start the game.");
			btnKickPlayer.setToolTipText("Only the admin can kick players.");
			btnInvitePlayer.setEnabled(false);
			btnKickPlayer.setEnabled(false);
			gameAdminLabel = new JLabel("");
		}
		gameAdminLabel.setForeground(Color.red);
		options.add(gameAdminLabel);
		
		JPanel chat = new JPanel();
		chat.setBounds(10, 51, 283, 115);
		getContentPane().add(chat);
		chat.setLayout(new GridLayout(1, 0, 0, 0));
		
		
		chatArea = new JTextArea();
		chatArea.setEditable(false);
		((DefaultCaret)chatArea.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		JScrollPane chatPane = new JScrollPane(chatArea);
		chatPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		chat.add(chatPane);

		
		JPanel input = new JPanel();
		input.setBackground(new Color(0, 100, 0));
		input.setBounds(10, 177, 283, 28);
		getContentPane().add(input);
		input.setLayout(null);
		
		JButton sendButton = new JButton("Send");
		sendButton.setFont(new Font("Tahoma", Font.PLAIN, 10));
		sendButton.setBounds(0, 0, 57, 28);
		sendButton.setVerticalAlignment(SwingConstants.BOTTOM);
		sendButton.setActionCommand("send");
		sendButton.addActionListener(this);
		input.add(sendButton);
		
		chatInput = new JTextField();
		chatInput.setBounds(67, 0, 216, 28);
		input.add(chatInput);
		chatInput.setColumns(10);
		
		JPanel header = new JPanel();
		header.setBorder(new LineBorder(new Color(255, 215, 0), 3, true));
		header.setBackground(new Color(139, 69, 19));
		header.setBounds(10, 11, 414, 29);
		getContentPane().add(header);
		
		JLabel gameTitle = new JLabel(adminUser.getName()+"'s "+game+" Game");
		gameTitle.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		header.add(gameTitle);
		
		gipLabel = new JLabel("");
		gipLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		gipLabel.setForeground(Color.red);
		header.add(gipLabel);
		
		JPanel playerList = new JPanel();
		playerList.setBounds(303, 51, 121, 154);
		getContentPane().add(playerList);
		playerList.setLayout(new GridLayout(0, 1, 0, 0));
		
		playerListArea = new JTextArea();
		playerListArea.setEditable(false);
		JScrollPane playerListPane = new JScrollPane(playerListArea);
		playerListPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		playerList.add(playerListPane);
		
		setTitle(adminUser.getName()+"'s "+game+" Game");
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setPreferredSize(new Dimension(450, 300));
		pack();
		setVisible(true);
		
		
		connectToServer();
	}
	
	private void connectToServer()
	{
		if(game.equals("Blackjack"))
			try
			{
				socket = new Socket(UserPass.IP_ADDRESS, 6000);
				oos = new ObjectOutputStream(socket.getOutputStream());
				if(isAdmin)
				{
					oos.writeObject(new GameAuthDatagram(isAdmin, UserPass.PLAYER, -1));
				}
				else
				{
					oos.writeObject(new GameAuthDatagram(isAdmin, UserPass.PLAYER, gid));
				}
				GameLobbyListenThread thread = new GameLobbyListenThread(this, socket);
				thread.start();
				oos.writeObject(new UserUpdateDatagram(UserPass.PLAYER));
			} catch (IOException e)
			{
				connectToServer();
				e.printStackTrace();
			}
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		if(e.getActionCommand().equals("send"))
		{
			sendDatagram(new ChatUpdateDatagram(UserPass.PLAYER.getName()+": "+chatInput.getText()));
			chatInput.setText("");
		}
		else
		{
			if(e.getActionCommand().equals("invite"))
			{
				new InvitePlayerFrame(this);
			}
			else
			{
				if(e.getActionCommand().equals("kick"))
					new KickPlayerFrame(this);
				else 
					if(e.getActionCommand().equals("start"))
					{
						System.out.println("GameAdmin sending GameStartDatagram!");
						sendDatagram(new GameStartDatagram(game));
					}
						
			}
		}
		
			
	}
	
	public void sendDatagram(Datagram gram)
	{
		try
		{
			oos.writeObject(gram);
		} catch (IOException e)
		{e.printStackTrace();}
	}
	
	public String getGame()
	{
		return game;
	}

	@Override
	public void updateChat(String s)
	{
		if(chatArea.getText().equals(""))
			chatArea.setText(s);
		else
			chatArea.setText(chatArea.getText() + "\n" + s);
	}

	@Override
	public String getChat() 
	{
		return chatArea.getText();
	}

	public void updateUsers(Object obj)
	{
		System.out.println("GameLobby updating userlist.");
		playerListArea.setText("");
		users = (Player[]) obj;
		for(Player user : users)
		{
			if(user.isAdmin())
				playerListArea.append(user.getName()+"*\n");
			else
				playerListArea.append(user.getName()+"\n");
		}
		buttonUpdate();
	}
	
	public void buttonUpdate()
	{
		if(isAdmin)
		{
			btnKickPlayer.setEnabled(true);
			if(users.length == reqPlayers)
			{
				btnStartGame.setEnabled(true);
				btnStartGame.setToolTipText("");
				btnInvitePlayer.setEnabled(false);
				btnInvitePlayer.setToolTipText("Max players reached.");
			}
			else
			{
				btnInvitePlayer.setEnabled(true);
				btnInvitePlayer.setToolTipText("");
				btnStartGame.setEnabled(false);
				btnStartGame.setToolTipText(game+" requires a minimum of "+reqPlayers+" players.");
			}
		}
	}

	public void setAdmin(boolean admin)
	{
		JOptionPane.showMessageDialog(this, "You have automatically been made game admin by the server.",
				"Game Admin Update", JOptionPane.INFORMATION_MESSAGE, null);
		isAdmin = true;
		buttonUpdate();
		gameAdminLabel.setText("GAME ADMIN");
	}

	public int getNumPlayers()
	{
		return users.length;
	}
	
	public int getReqPlayers()
	{
		return reqPlayers;
	}
	
	public void dispose()
	{
		super.dispose();
		try
		{
			new LobbyClient();
		} catch (UnknownHostException e)
		{e.printStackTrace();}
		catch (IOException e)
		{e.printStackTrace();}
	}

	/**
	 * @param game2
	 */
	public void startGame(String game2)
	{
		System.out.println(UserPass.PLAYER.getName()+" trying to start game!");
		new BlackClientOut(UserPass.PLAYER.getName());
		gipLabel.setText("IN PROGRESS");
	}
	
	
}
