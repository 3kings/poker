package client.gamelobby;

import global.dgframework.InviteDatagram;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;

import client.login.UserPass;

public class InvitePlayerFrame extends JFrame implements ActionListener
{
	private JTextField textField;
	private GameLobby parentLobby;
	
	public InvitePlayerFrame(GameLobby parent) 
	{
		setTitle("Invite Player(s)");
		setResizable(false);
		
		parentLobby = parent;
		
		getContentPane().setLayout(null);
		
		JLabel lblPlayers = new JLabel("Players");
		lblPlayers.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlayers.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPlayers.setBounds(10, 11, 46, 14);
		getContentPane().add(lblPlayers);
		
		textField = new JTextField();
		textField.setBounds(66, 9, 250, 20);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnSendInvites = new JButton("Send Invite(s)");
		btnSendInvites.addActionListener(this);
		btnSendInvites.setActionCommand("send");
		btnSendInvites.setBounds(66, 40, 120, 23);
		getContentPane().add(btnSendInvites);
		
		JButton btnRandomInvite = new JButton("Random Invite");
		btnRandomInvite.setToolTipText("Invite a random person from the main lobby.");
		btnRandomInvite.setBounds(196, 40, 120, 23);
		btnRandomInvite.addActionListener(this);
		btnRandomInvite.setActionCommand("rand");
		getContentPane().add(btnRandomInvite);
		
		setPreferredSize(new Dimension(364, 106));
		pack();
		setVisible(true);
	}
	
	private int numCommas(String s)
	{
		int commas = 0;
		for(int i = 0; i < s.length(); i++)
			if(s.charAt(i) == ',')
				commas++;
		return commas;
	}
	
	private ArrayList<String> makeList(String users)
	{
		ArrayList<String> list = new ArrayList<String>();
		int startIndex = 0;
		for(int i = 0; i < users.length(); i++)
			if(users.charAt(i) == ',')
			{
				list.add(users.substring(startIndex, i));
				startIndex = i + 2;
			}
			else
				if(i == users.length() - 1)
					list.add(users.substring(startIndex, users.length()));
		return list;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		if(e.getActionCommand().equals("send"))
		{
			int n = numCommas(textField.getText()) + parentLobby.getNumPlayers();
			if(n < parentLobby.getReqPlayers())
			{
				parentLobby.sendDatagram(new InviteDatagram(makeList(textField.getText()), UserPass.PLAYER.getName(), parentLobby.getGame(), -1));
				System.out.println("InviteDatagram sent by GameLobby.");
				dispose();
			}
			else
			{
				JOptionPane.showMessageDialog(this, "You have invited "+ (n - parentLobby.getReqPlayers() + 1) + " too many players.",
						"Too Many Invites", JOptionPane.INFORMATION_MESSAGE, null);
			}
		}
		else
		{
			if(e.getActionCommand().equals("rand"))
			{
				parentLobby.sendDatagram(new InviteDatagram(null, UserPass.PLAYER.getName(), parentLobby.getGame(), -1));
				dispose();
			}
		}
	}
}
