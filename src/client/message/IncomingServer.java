package client.message;

import java.net.*;

import javax.swing.JOptionPane;

public class IncomingServer extends Thread
{
	private ServerSocket serv;
	private Socket client;
	private boolean on = false;
	
	public IncomingServer()
	{
		super("IncomingServer");
		on = true;
	}
	
	public void run()
	{
		try
		{
			serv = new ServerSocket(15000);
			while(on)
			{
				client = serv.accept();
				new HandleNotification(client).start();
			}
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, "You are running more than one client or an error has occured");
			System.exit(0);
		}
	}
	
	/**
	 * Send it true to turn notifications on or false to turn notifications off
	 * @param a true for on false for off
	 */
	public void turnNotifications(boolean a)
	{this.on = a;}

}
