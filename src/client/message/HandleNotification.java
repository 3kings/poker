package client.message;

import java.io.*;
import java.net.Socket;

import javax.swing.JOptionPane;

import global.dgframework.Datagram;
import global.dgframework.DatagramHandler;

public class HandleNotification extends Thread
{
	private ObjectInputStream input;
	private ObjectOutputStream output;
	
	public HandleNotification(Socket client) throws IOException
	{
		output = new ObjectOutputStream(client.getOutputStream());
		input = new ObjectInputStream(client.getInputStream());
	}
	
	public void run()
	{
		try
		{
			DatagramHandler.parseDatagram((Datagram)input.readObject(), this);
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	/*TODO add a bunch of methods here to call back to
	 */
	
	public void showNotification(String message)
	{
		JOptionPane.showMessageDialog(null, "New Notification: \n"+message);
	}
}
