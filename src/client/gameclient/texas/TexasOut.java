package client.gameclient.texas;

import global.Player;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import client.gameclient.blackjack.BlackClientIn;
import client.gameclient.blackjack.BlackJack;
import client.login.UserPass;

public class TexasOut extends Thread
{
	private ArrayList<Player> players = new ArrayList<Player>();
	private Integer myLobbyPot = new Integer(0);
	private Integer lobbyTurn = new Integer(0);
	private Boolean[] myRestrictions = new Boolean[5];
	private Integer round = new Integer(0);
	
	private boolean con;
	
	private Socket socket = null;
	private ObjectOutputStream out = null;
	private ObjectInputStream in = null;
	
	private Integer myTurnId;
	
	private Player caught;
	private TexasHoldem game;
	
	public TexasOut(String userName)
	{
		super("TexasOut");
		connect(userName);
		new TexasIn(this,in).start();
		start();
	}
	
	private void connect(String userName)
	{
		try
		{
			//connects to the texas server and gets a port to connect to (which in turn is the lobby you will be in)
			socket = new Socket(UserPass.IP_ADDRESS, 7000);
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			Integer soc;
			soc = (Integer) in.readObject();
			out.flush();
			out.close();
			in.close();
			socket.close();
			//connects to that lobby and collects all the data you need to play
			socket = new Socket(UserPass.IP_ADDRESS,soc);
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			out.writeObject(userName);
			
			caught = (Player)in.readObject();
			players.add(caught);
			
			myRestrictions = (Boolean[])in.readObject();
			lobbyTurn = (Integer)in.readObject();
			myTurnId = (Integer) in.readObject();
			myLobbyPot = (Integer)in.readObject();
			round = (Integer)in.readObject();
			
			out.flush();
			
			con = true;
			//game = new TexasHoldem(caught,this);
			//game.setFrame();
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	public void setLobbyPot(Integer newPot)
	{this.myLobbyPot = newPot;}
	
	public Integer getLobbyPot()
	{return this.myLobbyPot;}
	
	public Integer getTurn()
	{return myTurnId;}
	
	public Boolean[] getMyRestrictions()
	{return this.myRestrictions;}

	public ArrayList<Player> getPlayers()
	{return players;}
	
	public void setLobbyTurn(Integer newTurn)
	{this.lobbyTurn = newTurn;}
	
	public Integer getLobbyTurn()
	{return this.lobbyTurn;}
}
