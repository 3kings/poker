package client.gameclient.texas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;

public class ActionFrame extends JFrame
{

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					ActionFrame frame = new ActionFrame(new Boolean[]{false,true,false,false,true});
					frame.setVisible(true);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param booleans 
	 */
	public ActionFrame(Boolean[] res)
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 295, 165);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 279, 126);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JButton btnCall = new JButton("Call");
		btnCall.setBounds(10, 11, 89, 23);
		panel.add(btnCall);
		
		if(res[0])
			btnCall.setEnabled(false);
		
		JButton btnRaise = new JButton("Raise");
		btnRaise.setBounds(10, 45, 89, 23);
		panel.add(btnRaise);
		
		if(res[1])
			btnRaise.setEnabled(false);
		
		JButton btnCheck = new JButton("Check");
		btnCheck.setBounds(10, 79, 89, 23);
		panel.add(btnCheck);
		
		if(res[2])
			btnCheck.setEnabled(false);
		
		JButton btnFold = new JButton("Fold");
		btnFold.setBounds(180, 11, 89, 23);
		panel.add(btnFold);
		
		JButton btnAllIn = new JButton("All in");
		btnAllIn.setBounds(180, 45, 89, 23);
		panel.add(btnAllIn);
		
		if(res[4])
			btnAllIn.setEnabled(false);
		
		JButton btnReserved = new JButton("reserved");
		btnReserved.setBounds(180, 79, 89, 23);
		panel.add(btnReserved);
		
		btnReserved.setEnabled(false);
		
		JLabel lblPleaseChooseAn = new JLabel("Please choose an Action to do");
		lblPleaseChooseAn.setBounds(64, 112, 177, 14);
		panel.add(lblPleaseChooseAn);
	}
}
