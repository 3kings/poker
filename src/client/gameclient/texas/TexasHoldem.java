package client.gameclient.texas;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import global.Player;

import javax.swing.*;

public class TexasHoldem extends JPanel
{
	private JFrame frame = new JFrame("Texas Hold'em");
	private JButton action = new JButton("Actions");
	
	private Player myPlayer;
	private TexasOut client;
	
	private Dimension buttons = new Dimension(110,25);
	private boolean playSound = false;
	
	public TexasHoldem(Player play, TexasOut client)
	{
		this.myPlayer = play;
		this.client = client;
	}
	
	public void setFrame()
	{
		
		action.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				new ActionFrame(client.getMyRestrictions());
			}
		});
	}
	
}
