package client.gameclient.blackjack;

import global.Player;

import java.io.ObjectInputStream;
import java.util.ArrayList;

public class BlackClientIn extends Thread
{
	private BlackClientOut parent;
	private ObjectInputStream input;
	
	
	public BlackClientIn(BlackClientOut parent, ObjectInputStream input)
	{
		super("BlackClientIn");
		this.parent = parent;
		this.input = input;
	}
	
	/**
	 * Waits for an update from the server and does the appropiate action
	 */
	public void run()
	{
		ArrayList<Object> caught;
		try
		{
			while(true)
			{
				caught = (ArrayList<Object>) input.readObject();
				switch((String)caught.get(0))
				{
					case "pot":
						parent.setLobbyPot((Integer)caught.get(1));
						break;
					case "turn":
						parent.setLobbyTurn((Integer)caught.get(1));
						break;
					case "player":
						parent.addTo((Player)caught.get(1));
						break;
				}
			}
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
}
