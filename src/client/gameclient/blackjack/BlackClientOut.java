package client.gameclient.blackjack;

import global.Player;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import client.login.UserPass;

public class BlackClientOut extends Thread
{
	private ArrayList<Player> players = new ArrayList<Player>();
	private Integer myLobbyPot = 0;
	private Integer lobbyTurn;
	
	private BlackJack game;
	
	private boolean con;
	
	private Socket socket = null;
	private ObjectOutputStream out = null;
	private ObjectInputStream in = null;
	
	private Integer myTurnId;
	
	private Player caught;
	
	/**
	 * Connects to the BlackJackServer and starts a game
	 * @param userName Username of player
	 */
	public BlackClientOut(String userName)
	{
		super("BlackClientOut");
		lobbyTurn = new Integer(0);
		myLobbyPot = new Integer(0);
		connect(userName);
		new BlackClientIn(this,in).start();
		start();
	}
	
	//first connects to server then lobby then game
	private void connect(String userName)
	{
		try
		{
			// connects to the black jack server where it will forward you to an already made lobby or a new one
			socket = new Socket(UserPass.IP_ADDRESS, 4000);
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			Integer soc;
			soc = (Integer) in.readObject();
			out.flush();
			out.close();
			in.close();
			socket.close();
			//connects to that lobby and collects all the data you need to play
			socket = new Socket(UserPass.IP_ADDRESS,soc);
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			out.writeObject(userName);
			
			caught = (Player)in.readObject();
			players.add(caught);
			
			myLobbyPot = (Integer)in.readObject();
			myTurnId = (Integer) in.readObject();
			lobbyTurn = (Integer)in.readObject();
			
			out.flush();
			
			con = true;
			game = new BlackJack(caught,this);
			game.setFrame();
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	//disconnects a player form the current game they are in
	public void disconnect(Player player)
	{
		try
		{
			out.writeObject(new Boolean(true));
			out.writeObject(player);
			con = false;
			socket.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	//ends players turn 
	public void endTurn()
	{
		ArrayList<Object> info = new ArrayList<Object>();
		try
		{
			info.add("stay");
			info.add(game.getMyPlayer());
			out.writeObject(info);
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	//writes new player's data to the server (when they click hit or stay)
	public void writeData(Player newData)
	{
		ArrayList<Object> info = new ArrayList<Object>();
		try
		{
			info.add("player");
			info.add(newData);
			info.add(myLobbyPot);
			out.writeObject(info);
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	//adds the player sent to the arraylist or if already in there it will replace the old with the new
	public void addTo(Player newInfo)
	{
		if(newInfo == null)
			return;
		String name = newInfo.getName();
		Player temp;
		String compareName;
		int indexToChange = -1;
		if(name.equalsIgnoreCase(game.getMyPlayer().getName()))
			game.updatePlayer(newInfo);
		for(int i = 0; i < players.size(); i++)
		{
			temp = players.get(i);
			compareName = temp.getName();
			if(compareName.equals(name))
			{
				indexToChange = i;
				break;
			}
		}
		if(indexToChange == -1)
			players.add(newInfo);
		else
			players.set(indexToChange, newInfo);
	}
	
	public void setLobbyPot(Integer newPot)
	{this.myLobbyPot = newPot;}
	
	public Integer getLobbyPot()
	{return this.myLobbyPot;}
	
	public Integer getTurn()
	{return myTurnId;}

	public ArrayList<Player> getPlayers()
	{return players;}
	
	public void setLobbyTurn(Integer newTurn)
	{this.lobbyTurn = newTurn;}
	
	public Integer getLobbyTurn()
	{return this.lobbyTurn;}
	
	public static void main(String[] commands)
	{
		
	}
}
