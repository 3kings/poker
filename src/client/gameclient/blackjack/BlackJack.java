package client.gameclient.blackjack;

import global.Cards;
import global.Player;

import javax.swing.*;

import org.omg.CORBA.Environment;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Scanner;

@SuppressWarnings("serial")
public class BlackJack extends JPanel
{
	//-125 from all variables (done)
	//Need to create Players and make their info display on screen.
	private int cardCount = 0, times = 0,myTurn = 0;
	private final int BASE = 490; //BASE = HitXLocation - HitWidth - 25(spacing)
	private int y = 565;
	private Cards card = new Cards(true);
	private JFrame frame = new JFrame("BlackJack");
	private JButton hit = new JButton("Hit");
	private JButton disc = new JButton("Disconnect");
	private JButton stay = new JButton("Stay");
	private JLabel label = new JLabel("Lobby 1");
	private Dimension buttons = new Dimension(110,25);
	
	private Player myPlayer;
	private BlackClientOut b;
	
	/**
	 * 
	 * @param play Sent the current player is playing
	 * @param client Sent the output to the server
	 */
	public BlackJack(Player play, BlackClientOut client)
	{
		myPlayer = play;
		b = client;
		myTurn = b.getTurn();
	}
	
	//paints all the stuff
	public void paintComponent(Graphics art)
	{
		art.clearRect(0, 0, 800,700);
		//paints the boxes which are highlighted by red or green
		paintTurnBoxes(art);
		art.setColor(Color.black);
		//paints all the other players
		paintPlayers(art);
		int count = 0;
		y = 565;
		for(int i = 0; i < myPlayer.getHand().size(); i++)   //paints your hand onto the screen (Always on bottom)
		{
			myPlayer.getHand().get(i).paintIcon(this, art, (BASE-(75*count)), y);
			count++;
			if(count == 6)
			{
				y = 535; // -30 from original y
				if(times == 0)
					cardCount = 0;
				count = 0;
				times++;
			}
		}
		//clears the rectangles so it looks less choppy
		art.clearRect(660, 618, 100, 20); // y = (Chip's y)-15
		art.clearRect(350, 285, 100, 20);
		art.clearRect(660, 593, 100, 20); // y = (Player's y)-15
		art.clearRect(660, 643, 150, 20); // y = (Ip's y)-15
		//draws your players info
		art.drawString("Player: "+myPlayer.getName(), 660, 608); //bottom players x = Hit + HitWidth(60) + 25, y = HitYLocation
		art.drawString("Chips: "+myPlayer.getChips() , 660, 633); // y = (The String Player's y)+25
		art.drawString("IP: "+myPlayer.getIP(),660,658);// y = (The String Chip's y)+25
		art.drawString("Pot: "+b.getLobbyPot(), 350, 300);
		repaint();
	}
	
	//paints all the turn boxes (green/red rectangular boxes)
	public void paintTurnBoxes(Graphics art)
	{
		int turn = b.getLobbyTurn();
		art.drawRect(660, 583, 75, 5); //your player (bottom)
		art.drawRect(700, 65, 75, 5); //top-right player (right)
		art.drawRect(100, 25, 75, 5); //top player (up)
		art.drawRect(5, 65, 75, 5); //top-left player (left)
		art.setColor(Color.red);
		art.fillRect(660, 583, 75, 25);
		art.fillRect(700, 65, 75, 5);
		art.fillRect(100, 25, 75, 5);
		art.fillRect(5, 65, 75, 5);
		//player rect
		art.setColor(Color.GREEN);
		if(turn == myTurn)
		{
			art.fillRect(660, 583, 75, 25);
			return;
		}
		switch(myTurn)
		{
			case 0:
				switch(turn)
				{
					case 1:
						art.fillRect(700,65,75,5);
						break;
					case 2:
						art.fillRect(100, 25, 75, 5);
						break;
					case 3:
						art.fillRect(5, 65, 75, 5);
						break;
				}
				return;
			case 1:
				switch(turn)
				{
					case 0:
						art.fillRect(700,65,75,5);
						break;
					case 2:
						art.fillRect(100, 25, 75, 5);
						break;
					case 3:
						art.fillRect(5, 65, 75, 5);
						break;
				}
				return;
			case 2:
				switch(turn)
				{
					case 0:
						art.fillRect(700,65,75,5);
						break;
					case 1:
						art.fillRect(100, 25, 75, 5);
						break;
					case 3:
						art.fillRect(5, 65, 75, 5);
						break;
				}
				return;
			case 3:
				switch(turn)
				{
					case 0:
						art.fillRect(700,65,75,5);
						break;
					case 1:
						art.fillRect(100, 25, 75, 5);
						break;
					case 2:
						art.fillRect(5, 65, 75, 5);
						break;
				}
				return;
				default:
					return;
		}
	}
	
	//updates your current player to the most up to date player
	public void updatePlayer(Player player)
	{
		myPlayer = player;
		cardCount = myPlayer.getHand().size();
	}
	
	//paints all the players except you
	public void paintPlayers(Graphics art)
	{
		ArrayList<Player> allExcept = b.getPlayers();
		int myIndex = this.getMyIndex(allExcept).intValue();
		Player myInfo;
		myInfo = allExcept.remove(myIndex);  //gets all the players in the lobby and then removes you becuase your always on the bottom
		Player info;
		for(int i = 0; i < allExcept.size(); i++) //goes through all the players
		{
			info = allExcept.get(i);
			switch(i) //paints their info
			{
			case 0:
				art.drawString("Player: "+info.getName(), 700, 90);
				art.drawString("Chips: "+info.getChips(), 700, 115);
				art.drawString("IP: "+info.getIP(), 700, 140);
				break;
			case 1:
				art.drawString("Player: "+info.getName(), 100, 50);
				art.drawString("Chips: "+info.getChips(), 100, 75);
				art.drawString("IP: "+info.getIP(), 100, 100);
				break;
			case 2:
				art.drawString("Player: "+info.getName(), 5, 90);
				art.drawString("Chips: "+info.getChips(), 5, 115);
				art.drawString("IP: "+info.getIP(), 5, 140);
				break;
			}
			
			int count = 0;  //sets the x's and y's
			int myHandCount = info.getHand().size()-1;
			int playY = 0;
			int myX = 0;
			switch(i)
			{
			case 0:
				playY = 140;
				myX = 700;
				break;
			case 1:
				playY = 25;
				myX = 215;
				break;
			case 2:
				playY = 140;
				myX = 5;
				break;
			}
			for(int x = 0; x < myHandCount+1; x++) //paints all the players cards (only shows one)
			{
				switch(i)
				{
				case 0:
				case 2:
					if(x < 1)
						info.getHand().get(x).paintIcon(this, art, (myX), (playY+(75*count)));
					else
						card.getBackCard().paintIcon(this, art, (myX), (playY+(75*count)));
					break;
				case 1:
					if(x < 1)
						info.getHand().get(x).paintIcon(this, art, (myX+(75*count)), playY);
					else
						card.getBackCard().paintIcon(this, art, (myX+(75*count)), playY);
					break;
				}
				count++;
				if(count == 6)
				{
					switch(i)
					{
					case 0:
					case 2:
						myX -= 75;
						break;
					case 1:
						playY = 155;
						break;
					} // -30 from original y
					count = 0;
				}
			}
		}
		allExcept.add(myInfo);
	}
	
	//gets your index of the player in the lobby
	public Integer getMyIndex(ArrayList<Player> allPlayers)
	{
		String myName = myPlayer.getName();
		for(int i = 0; i < allPlayers.size(); i++)
		{
			if(myName.equalsIgnoreCase(allPlayers.get(i).getName()))
				 return i;
		}
		return -1;
	}
	
	//makes the frame nothing special (TODO add more graphics)
	public void setFrame()
	{
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBackground(Color.white);
		hit.setSize(buttons);
		stay.setSize(buttons);
		disc.setSize(buttons);
		
		this.setPreferredSize(new Dimension(800,700));
		frame.setPreferredSize(new Dimension(800,700));
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setResizable(false);
		this.setLayout(new GroupLayout(this));
		
		this.add(label);
		this.add(hit);
		this.add(stay);
		this.add(disc);
		
		disc.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				//b.disconnect(myPlayer); 
			}
		});
		
		hit.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				if(b.getLobbyTurn() == myTurn) //checks to see if it is even your turn
				{
					if(cardCount < 6)
					{
						//if your hand count is below 21 it will add a card to your hand and then subtract from your chips.
						//also updates the lobby's pot and then sends over your up to date player to the server (and pot)
						if(getHandCount(myPlayer.getHand()) < 21) 
						{
							myPlayer.addToHand(card.getRandomCard());
							myPlayer.setChips(myPlayer.getChips()-50);
							b.setLobbyPot(b.getLobbyPot()+50);
							b.writeData(myPlayer);
							if(cardCount == 0 && times == 1)
								cardCount = 0;
							else
								cardCount++;
						}
						else
						{
							b.endTurn();
						}
					}
				}
			}
		});
		stay.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				if(b.getLobbyTurn() == myTurn) //ends your turn if it your turn
					b.endTurn();
			}
		});
		
		label.setFont(new Font("Tahoma",10,11));
		label.setLocation(new Point(250,0));
		hit.setLocation(new Point(575,608));
		stay.setLocation(new Point(575,640));
		disc.setLocation(new Point(680,640));
		
		disc.setBounds(680,640,110,25);
		hit.setBounds(575, 608, 60, 25);
		stay.setBounds(575, 640, 60, 25);
		label.setBounds(250, 0, 50, 25);
		this.setBounds(0, 0, 540, 460);
		
		this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
		frame.getContentPane().add(this);
		frame.pack();
		frame.setVisible(true);
	}
	
	//gets the integer values of the cards in your hand
	public Integer getHandCount(ArrayList<ImageIcon> arrayList)
	{
		Integer count = 0;
		for(int i = 0; i < arrayList.size(); i++)
			count += Cards.getFaceValue(arrayList.get(i).getDescription());
		return count;
	}

	public Player getMyPlayer()
	{
		return myPlayer;
	}
}
