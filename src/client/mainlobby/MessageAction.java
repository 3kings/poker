package client.mainlobby;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.text.DefaultCaret;

import client.information.Information;
import global.dgframework.MessageDatagram;

public class MessageAction extends Thread
{
	private JFrame frame = new JFrame("Message");
	private JPanel panel = new JPanel();
	private JTextArea area = new JTextArea();
	private JScrollPane pane = new JScrollPane(area);
	private JLabel where = new JLabel("Sending to: ");
	private JLabel sender = new JLabel("Your Name: ");
	private JButton button = new JButton("Send");
	
	public MessageAction(final String who, final String toWho)
	{
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setPreferredSize(new Dimension(200,300));
		panel.setPreferredSize(new Dimension(200,300));
		pane.setPreferredSize(new Dimension(185,180));
		
		area.setLineWrap(true);
		
		where.setText(where.getText()+toWho);
		sender.setText(sender.getText()+who);
		
		panel.add(where);
		panel.add(sender);
		panel.add(pane);
		panel.add(button);
		
		button.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				if(area.getText().length() > 100)
				{
					new LessFrame();
					return;
				}
				MessageDatagram gram = new MessageDatagram(toWho,who,"Default",area.getText());
				Information info = new Information(gram);
				info.start();
				frame.dispose();
			}
		});
		
		((DefaultCaret)area.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setVisible(true);
		
		start();
	}

	public static void main(String[] args)
	{
		MessageAction a = new MessageAction("admin","chad");
	}

}
