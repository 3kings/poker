package client.mainlobby;

import global.Player;
import global.dgframework.Chat;
import global.dgframework.ChatUpdateDatagram;
import global.dgframework.PlayersDatagram;
import global.dgframework.StatDatagram;
import global.dgframework.UserUpdateDatagram;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.*;
import javax.swing.text.DefaultCaret;

import client.information.Information;
import client.login.UserPass;
import client.message.Inbox;
import client.message.IncomingServer;
import client.popups.SelectUserFrame;
import client.stats.GetPlayersFrame;
import client.stats.StatFrame;

public class LobbyClient implements ActionListener, Chat
{
	private JFrame frame;
	private JPanel panel, chat, userListPanel, options;
	private JTextArea chatArea;
	private JTextField chatInput;
	private JScrollPane chatSP;
	private JButton sendInput, newGame, players, message, inbox;
	private ObjectOutputStream oos;
	private Socket socket;
	private boolean connectionClosed;
	
	
	private JList<String> list;
	private JScrollPane usersList;
	private DefaultListModel<String> list1;
	
	public LobbyClient() throws UnknownHostException, IOException
	{
		connectionClosed = false;
		
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setPreferredSize(new Dimension(540, 355));
		frame.getRootPane().setDefaultButton(sendInput);
		frame.setResizable(false);
		
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(540, 355));
		panel.setBackground(Color.red);
		
		chat = new JPanel();
		chat.setBackground(Color.green);
		chat.setPreferredSize(new Dimension(300, 200));
		
		chatArea = new JTextArea();
		chatArea.setEditable(false);
		((DefaultCaret)chatArea.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		chatSP = new JScrollPane(chatArea);
		chatSP.setPreferredSize(new Dimension(290, 150));
		chatSP.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		sendInput = new JButton("Send");
		sendInput.setPreferredSize(new Dimension(65, 30));
		sendInput.setActionCommand("send");
		sendInput.addActionListener(this);
		
		chatInput = new JTextField();
		chatInput.setPreferredSize(new Dimension(220, 30));
		chatInput.setActionCommand("send");
		chatInput.addActionListener(this);
		
		chat.add(chatSP);
		chat.add(sendInput);
		chat.add(chatInput);
		
		userListPanel = new JPanel();
		userListPanel.setBackground(Color.blue);
		userListPanel.setPreferredSize(new Dimension(200, 200));
		
		list1 = new DefaultListModel<String>();
		
		list = new JList<String>(list1);
		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setVisibleRowCount(-1);
		
		usersList = new JScrollPane(list);
		usersList.setPreferredSize(new Dimension(190,190));
		
		userListPanel.add(usersList);
		
		options = new JPanel();
		options.setBackground(Color.yellow);
		options.setPreferredSize(new Dimension(500, 100));
		
		newGame = new JButton("New Game");
		newGame.setPreferredSize(new Dimension(100, 25));
		newGame.setActionCommand("newGame");
		newGame.addActionListener(this);
		
		players = new JButton("All Players");
		players.setPreferredSize(new Dimension(100,25));
		players.setActionCommand("players");
		players.addActionListener(this);
		
		message = new JButton("New Message");
		message.setPreferredSize(new Dimension(150,25));
		message.setActionCommand("message");
		message.addActionListener(this);
		
		inbox = new JButton("My Messages");
		inbox.setPreferredSize(new Dimension(150,25));
		inbox.setActionCommand("inbox");
		inbox.addActionListener(this);
		
		options.add(newGame);
		options.add(players);
		options.add(message);
		options.add(inbox);
		
		panel.add(chat);
		panel.add(userListPanel);
		panel.add(options);
		
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setVisible(true);
		
		connectToLobbyServer();
	}
	
	private void connectToLobbyServer()
	{
		try
		{
			chatInput.setEditable(false);
			this.socket = new Socket(UserPass.IP_ADDRESS, 5002);
			oos = new ObjectOutputStream(socket.getOutputStream());
			new LobbyClientThread(this, socket).start();
			sendUserUpdate();
			chatInput.setEditable(true);
			
		} catch (UnknownHostException e)
		{
			System.out.println("Client opening unknown host popup.");
			if(!connectionClosed)
				serverNotFoundPopup();
		} catch (IOException e)
		{
			System.err.println("IOException: LobbyClient.java");
			if(!connectionClosed)
				serverNotFoundPopup();
		}
	}
	
	/* Popup that appears when the lobby server cannot be reached.  Returns false if the user chooses to 
	reconnect and true if the user chooses to exit (used for ending associated LobbyClientThread). */
	public boolean serverNotFoundPopup()
	{
		int n = JOptionPane.showOptionDialog(frame, "Unable to connect to lobby server.  Retry?", "Server Not Found",
				JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, null, JOptionPane.YES_OPTION);
		if(n == JOptionPane.YES_OPTION)
		{
			connectToLobbyServer();
			return false;
		}
		else
			if(n == JOptionPane.CLOSED_OPTION || n == JOptionPane.NO_OPTION)
			{
				frame.dispose();
				System.exit(1);
			}
		return true;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		switch(e.getActionCommand())
		{
			case "newGame":
				new NewGameFrame(UserPass.PLAYER, this);
				break;
			case "send":
				if(chatInput.getText().length() >= 2)
					sendMessage();
				else
					chatInput.setText("");
				break;
			case "players":
				try
				{
					Information a = new Information(new PlayersDatagram(null));
					a.start();
					a.join();
					new GetPlayersFrame(((PlayersDatagram)a.getDatagram()).getNames(), UserPass.PLAYER.getName()).setFrame();
				}
				catch(Exception b)
				{b.printStackTrace();}
				break;
			case "message":
				new Message(UserPass.PLAYER.getName());
				break;
			case "inbox":
				try
				{
					Information a = new Information(new StatDatagram(UserPass.PLAYER.getName()));
					a.start();
					a.join();
					new Inbox(((StatDatagram)a.getDatagram()).getPlayer().getMessages(), UserPass.PLAYER.getName());
				}
				catch(Exception b)
				{b.printStackTrace();}
				break;
		}
	}
	
	//when you connect to the server this is sent to ensure you are put in
	private void sendUserUpdate() throws IOException
	{
		oos.writeObject(new UserUpdateDatagram(UserPass.PLAYER));
	}
	
	private void sendMessage()
	{
		try
		{
			oos.writeObject(new ChatUpdateDatagram(UserPass.PLAYER.getName()+": "+chatInput.getText()));
			chatInput.setText("");
		} catch (IOException e1)
		{
			e1.printStackTrace();
		}
	}

	@Override
	public void updateChat(String s)
	{
		if(chatArea.getText().equals(""))
			chatArea.setText(s);
		else
			chatArea.setText(chatArea.getText() + "\n" + s);
	}

	@Override
	public String getChat()
	{
		return chatArea.getText();
	}
	
	public void updateUser(Player[] obj)
	{
		list1.removeAllElements();
		for(int x = 0; x < obj.length; x++)
			list1.addElement(obj[x].getName());
	}
	
	public void dispose()
	{
		connectionClosed = true;
		frame.dispose();
		try
		{
			socket.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public boolean connectionClosed()
	{
		return connectionClosed;
	}


}
