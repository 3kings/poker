package client.mainlobby;

import global.Player;

import javax.swing.JFrame;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

import client.gamelobby.GameLobby;

public class NewGameFrame extends JFrame implements ActionListener
{
	
	private JComboBox comboBox;
	private Player user;
	private LobbyClient parent;
	
	public NewGameFrame(Player user2, LobbyClient parent) 
	{
		setResizable(false);
		
		this.user = user2;
		this.parent = parent;
		
		getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblGame = new JLabel("Game:");
		lblGame.setFont(new Font("Tahoma", Font.BOLD, 12));
		getContentPane().add(lblGame);
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Blackjack", "Texas Hold'em", "Hearts", "Spades"}));
		comboBox.setSelectedIndex(0);
		getContentPane().add(comboBox);
		
		JButton btnStartGame = new JButton("Start Game");
		btnStartGame.addActionListener(this);
		getContentPane().add(btnStartGame);
		
		setPreferredSize(new Dimension(150, 150));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		pack();
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		new GameLobby((String)comboBox.getSelectedItem(), true, -1, user);
		dispose();
		parent.dispose();
	}

}
