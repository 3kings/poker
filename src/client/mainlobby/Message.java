package client.mainlobby;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;

import client.information.Information;
import global.dgframework.MessageDatagram;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;

public class Message extends JFrame
{
	private JPanel contentPane;
	private JTextField users;
	private JTextField subject;
	private int clicks = 0;
	
	public static void main(String[] args)
	{
		Message a = new Message("admin");
	}

	/**
	 * Create the frame.
	 */
	public Message(final String from)
	{
		this.setTitle("Compose New Message");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 500, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
		);
		
		JLabel lblTo = new JLabel("To: ");
		
		users = new JTextField();
		users.setColumns(10);
		
		users.setText("For Multiple Users Seperate by Just Commas");
		
		JLabel lblFrom = new JLabel("From: "+from);
		
		JLabel lblSubject = new JLabel("Subject: ");
		
		subject = new JTextField();
		subject.setColumns(10);
		
		final JTextArea area = new JTextArea();
		area.setText("Max 200 words! It will not tell you your current word count. Please be cautious!");
		JScrollPane pane = new JScrollPane(area);
		
		area.setLineWrap(true);
		area.setWrapStyleWord(true);
		
		JScrollPane scrollPane = new JScrollPane();
		
		area.addFocusListener(new FocusListener()
		{
			public void focusGained(FocusEvent e)
			{
				if(clicks == 0)
					area.setText("");
				if(clicks >= 1)
					return;
				clicks++;
			}
			public void focusLost(FocusEvent arg0)
			{
				
			}
		});
		
		JButton send = new JButton("Send");
		send.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				if(area.getText().split(" ").length > 200)
				{
					new LessFrame();
					return;
				}
				MessageDatagram gram = new MessageDatagram(users.getText(),from,subject.getText(),area.getText());
				Information info = new Information(gram);
				info.start();
				dispose();
			}
		});
		
		JButton discard = new JButton("Discard");
		discard.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				dispose();
			}
		});
		
		JButton saveG = new JButton("Save Group");
		saveG.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				
			}
		});
		
		JButton addG = new JButton("Add Group");
		addG.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				//StatGetter a = new StatGetter(from);
				//Player myPlayer = a.getStats();
				// new frame which will display which one to select (send it myself so it can update the textfield when you do select one)
			}
		});
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblTo)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(users, GroupLayout.DEFAULT_SIZE, 431, Short.MAX_VALUE))
						.addComponent(lblFrom)
						.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
								.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
									.addComponent(send, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(discard, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(addG, GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
									.addGap(18)
									.addComponent(saveG)
									.addPreferredGap(ComponentPlacement.RELATED))
								.addComponent(pane, Alignment.LEADING)
								.addGroup(gl_panel.createSequentialGroup()
									.addComponent(lblSubject)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(subject, GroupLayout.DEFAULT_SIZE, 405, Short.MAX_VALUE)))
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTo)
						.addComponent(users, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblFrom)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSubject)
						.addComponent(subject, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(81)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(pane, GroupLayout.DEFAULT_SIZE, 327, Short.MAX_VALUE)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(send)
						.addComponent(saveG)
						.addComponent(addG)
						.addComponent(discard))
					.addGap(7))
		);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
		this.setVisible(true);
	}
}
