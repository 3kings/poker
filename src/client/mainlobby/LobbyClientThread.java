package client.mainlobby;

import global.Player;
import global.dgframework.Chat;
import global.dgframework.Datagram;
import global.dgframework.DatagramHandler;
import global.dgframework.UserHolder;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.*;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import client.gamelobby.GameLobby;
import client.login.UserPass;

public class LobbyClientThread extends Thread implements Chat, UserHolder
{
	
	private LobbyClient parent;
	private ObjectInputStream ois;
	private boolean on = true;
	private Socket socket;
	
	public LobbyClientThread(LobbyClient parent, Socket socket) throws IOException
	{
		super("LobbyClientThread");
		this.parent = parent;
		this.socket = socket;
		ois = new ObjectInputStream(socket.getInputStream());
	}
	
	public void run()
	{
		while(on)
		{
			try
			{
				DatagramHandler.parseDatagram((Datagram)ois.readObject(), this);
				if(!socket.isConnected())
					if(!parent.connectionClosed())
					{
						parent.serverNotFoundPopup();
						return;
					}
			} catch (IOException e)
			{
				if(!parent.connectionClosed())
					on = parent.serverNotFoundPopup();
			} catch (ClassNotFoundException e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public void updateChat(String s)
	{
		parent.updateChat(s);
	}
	

	@Override
	public String getChat()
	{
		return parent.getChat();
	}

	@Override
	public void updateUsers(Object obj)
	{
		parent.updateUser((Player[]) obj);
	}

	public void userBanned(String reason, boolean ban)
	{
		if(ban)
			JOptionPane.showMessageDialog(null, "You have been banned for the reason: "+reason, "User Banned", JOptionPane.INFORMATION_MESSAGE);
		else
			JOptionPane.showMessageDialog(null, "You have been kicked for the reason: "+reason, "User Kicked", JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
	}

	public void displayInvite(String sendingUser, String game, int gid)
	{
		int n = JOptionPane.showOptionDialog(null, sendingUser+" would like to play "+game+ " with you.  Would you like to play?",
				"Invite from "+sendingUser, JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, JOptionPane.YES_OPTION);
		if(n == JOptionPane.YES_OPTION)
		{
			new GameLobby(game, false, gid, new Player(sendingUser, null));
			System.out.println("GID is now "+gid);
			parent.dispose();
		}
	}
	
}
