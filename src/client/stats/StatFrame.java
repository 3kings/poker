package client.stats;

import java.awt.*;
import javax.swing.*;
import global.Player;
import global.dgframework.StatDatagram;

public class StatFrame extends JPanel
{
	private JFrame frame = new JFrame();
	private Color admin = new Color(204,204,0);
	private Color mod = new Color(0,204,102);
	
	private Player player;
	
	//The player that you selected
	public StatFrame(StatDatagram gram)
	{
		this.player = gram.getPlayer();
		
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setPreferredSize(new Dimension(275,200));
		this.setPreferredSize(new Dimension(275,200));
		
		frame.getContentPane().add(this);
		frame.pack();
		frame.setVisible(true);
	}
	
	public void paintComponent(Graphics g)
	{
		g = (Graphics2D)g;
		if(player.getName() != null)
		{
			if(player.getAvatar() == null)
			{
				g.setColor(Color.red);
				g.drawRect(160, 40, 75, 75);
				g.setColor(Color.black);
				g.drawString("No Avatar", 175, 85);
			}
			else
			{
				resize(player.getAvatar(),75,75,false).paintIcon(this, g, 160, 40);
			}
			g.setFont(new Font("Helvetica", Font.ITALIC, 15));
			g.drawString("Name: "+player.getName(), 10, 55);
			g.drawString("Chip Count: "+player.getChips(), 10, 70);
			g.drawString("IP: "+player.getIP(), 10, 85);
		//	g.drawString("Current Hand: "+player.getHand(), 10, 100);
			if(player.isAdmin())
			{
				g.setColor(admin);
				g.drawString("Administrator", 155, 35);
			}
			else
			{
				if(player.isMod())
				{
					g.setColor(mod);
					g.drawString("Moderator", 155+7, 35);
				}
				else
				{
					g.setColor(Color.ORANGE);
					g.drawString("Player", (155+23), 35);
				}
			}
			if(player.isOnline())
			{
				g.setColor(Color.GREEN);
				g.drawString("Currently Online: "+player.isOnline(), 10, 115);
			}
			else
			{
				g.setColor(Color.red);
				g.drawString("Currently Online: "+player.isOnline(), 10, 115);
			}
			g.setColor(Color.black);
			//other stuff maybe
		}
		
	}
	//resizes their image icon to w.e. size (75,75) for this frame
	public ImageIcon resize(ImageIcon imageIcon, int width, int height, boolean max) 
	{
		Image image = imageIcon.getImage();
		Image newimg = image.getScaledInstance(-1, height, java.awt.Image.SCALE_SMOOTH);
		int width1 = newimg.getWidth(null);
		if ((max && width1 > width) || (!max && width1 < width))
		    newimg = image.getScaledInstance(width, -1, java.awt.Image.SCALE_SMOOTH);
		return new ImageIcon(newimg);
	}
}
