package client.stats;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

import client.information.Information;
import client.login.UserPass;
import client.mainlobby.MessageAction;
import client.message.Inbox;
import client.popups.SelectUserFrame;
import global.dgframework.StatDatagram;

public class GetPlayersFrame extends Thread
{
	private JFrame frame = new JFrame();
	private JPanel panel = new JPanel();
	
	private DefaultListModel<String> list1 = new DefaultListModel<String>();
	private JList<String> list;
	private JScrollPane pane;
	
	
	private JButton getStat = new JButton("GetStats");
	
	private ArrayList<String> names;
	private String myName;
	
	/**
	 * 
	 * @param names Player's Names
	 * @param myName Your name which is used for message button
	 */
	public GetPlayersFrame(ArrayList<String> names, String myName)
	{
		super("GetPlayersFrame");
		this.names = names;
		this.myName = myName;
	}
	
	//adds all the names to the list model
	public void addNames()
	{
		for(String name : names)
			list1.addElement(name);
	}
	
	//sets frame
	public void setFrame()
	{
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setPreferredSize(new Dimension(200,275));
		panel.setPreferredSize(new Dimension(200,275));
		
		addNames();
		list = new JList<String>(list1);
		pane = new JScrollPane(list);
		
		pane.setPreferredSize(new Dimension(180,160));
		
		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION); //single selection only
		list.setLayoutOrientation(JList.VERTICAL); //only vertical
		list.setVisibleRowCount(-1);
		
		getStat.setPreferredSize(new Dimension(90,25));
		
		getStat.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Information a;
				if(list.getSelectedIndex() != -1)
				{
					a = new Information(new StatDatagram(list.getSelectedValue()));
					a.start();
					try
					{
						a.join();
					}
					catch(Exception b)
					{b.printStackTrace();}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Please select a user from the list");
					return;
				}
				new StatFrame((StatDatagram)a.getDatagram());
			}
		});
		
		panel.add(pane);
		panel.add(getStat);
		
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setVisible(true);
	}
	
	public static void main(String[] args)
	{
	}

}
