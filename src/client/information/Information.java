package client.information;

import java.io.*;
import java.net.Socket;

import client.login.UserPass;
import global.dgframework.Datagram;

public class Information extends Thread
{
	private Datagram gram;
	private Datagram caught;
	private Socket client;
	private ObjectOutputStream out;
	private ObjectInputStream in;
	boolean running = false;
	
	public Information(Datagram gram)
	{
		this.gram = gram;
		this.running = true;
	}
	
	public void run()
	{
		try
		{
			client = new Socket(UserPass.IP_ADDRESS, 5001);
			out = new ObjectOutputStream(client.getOutputStream());
			in = new ObjectInputStream(client.getInputStream());
			out.writeObject(gram);
			caught = (Datagram) in.readObject();
		}
		catch(Exception e)
		{e.printStackTrace();}
	}
	
	public Datagram getDatagram()
	{
		return this.caught;
	}
	
}
