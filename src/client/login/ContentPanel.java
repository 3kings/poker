package client.login;

import global.MD5Hash;
import global.Player;
import global.dgframework.CredenDatagram;
import global.dgframework.StatDatagram;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.Random;

import javax.swing.*;

import client.information.Information;
import client.mainlobby.LobbyClient;

public class ContentPanel extends JPanel implements MouseListener,KeyListener
{
	private static final long serialVersionUID = 940940223518521626L; //do not remove
	
	private StarPolygon a;
	private JFrame parent;
	private UserPass superClass;
	
	private int stage = 0;
	private int subStage = 0;
	
	//login section stage = 1 subStage = 0/1
	private Rectangle allRec = new Rectangle(0,0,this.getWidth(),this.getHeight());
	private Rectangle button = new Rectangle(160,25); //width/height
	private Point logRec = new Point(100,110);
	private Point regRec = new Point(100,140);
	private Rectangle log = new Rectangle(logRec.x,logRec.y,button.width,button.height);
	private Rectangle reg = new Rectangle(regRec.x,regRec.y,button.width,button.height);
	private Random gen = new Random();
	private JTextField user = new JTextField();
	private JPasswordField pass = new JPasswordField();
	private JButton login = new JButton("Login");
	private JButton register = new JButton("Register");
	
	//register section stage = 2 subStage = 0/1
	private JTextField username = new JTextField();
	private JPasswordField password = new JPasswordField();
	private JPasswordField repass = new JPasswordField();
	private JButton avatar = new JButton("Avatar");
	private JCheckBox hide = new JCheckBox("Hide ip?");
	private JButton logButton = new JButton("Login Screen");
	private JButton mainButton = new JButton("Main Screen");
	private JButton regButton = new JButton("Submit");
	
	//For the pop ups
	private String inUserandPass = "Incorrect Username or Password. Please Try Again";
	private String inPassword = "Incorrect Password! Please try again.";
	private String parseError = "Please make sure your username or password does not contain a space";
	private String takenUsername = "Username Already Taken Please Try a New One";
	
	private final int GAP = 5;
	private Socket client;
	private ImageIcon myAva;
	private ContentPanel me;
	
	public ContentPanel(JFrame parent, UserPass superClass)
	{
		super();
		this.parent = parent;
		this.superClass = superClass;
		this.addMouseListener(this);
		hide.setEnabled(false);
		this.me = this;
	}
	
	private void drawRect(Rectangle x, Graphics g)
	{
		g.drawRect(x.x,x.y,x.width,x.height);
	}
	
	private void drawRect(int x, int y, Rectangle rec, Graphics g)
	{
		g.drawRect(x, y, (int)rec.getWidth(), (int)rec.getHeight());
	}
	
	public void unhideFields()
	{
		user.setSize(150, 20);
		pass.setSize(150,20);
		login.setSize((int)user.getSize().getWidth()/2,(int)user.getSize().getHeight());
		register.setSize((int)user.getSize().getWidth()/2,(int)user.getSize().getHeight());
		
		user.setLocation(120,135);
		user.setBounds(user.getX(), user.getY(), (int)user.getSize().getWidth(),  (int)user.getSize().getHeight());
		
		pass.setLocation(120,165);
		pass.setBounds(pass.getX(), pass.getY(), (int)pass.getSize().getWidth(), (int)pass.getSize().getHeight());
		
		login.setLocation(user.getX(), (user.getY()+15+user.getBounds().height+pass.getBounds().height)); //10 of the 15 is the small gap between them 5 of it is a made gap
		login.setBounds(login.getX(), login.getY(), login.getWidth(), login.getHeight());
		
		register.setLocation((int)(login.getX()+login.getSize().getWidth()), (int)(login.getY()));
		register.setBounds(register.getX(), register.getY(), register.getWidth(), register.getHeight());
		
		user.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				login.doClick();
			}
		});
		pass.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				login.doClick();
			}
		});
		login.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				if(UserPass.usingJar)
				{
					if(checkUpdate())
					{
						JOptionPane.showMessageDialog(null, "Please make sure you are running the Updater.jar first");
						return;
					}
				}
				
				try
				{
					Login b = new Login(new CredenDatagram(user.getText(), pass.getText(), InetAddress.getLocalHost().getHostAddress(), false));
					b.start();
					b.join();
					int valid = b.valid();
					switch(valid)
					{
						case 0:
							Information a = new Information(new StatDatagram(user.getText()));
							a.start();
							try
							{
								a.join();
							}
							catch(Exception e)
							{e.printStackTrace();}
							StatDatagram q = (StatDatagram) a.getDatagram();
							superClass.setPlayer(q.getPlayer());
							UserPass.PLAYER.setIP(InetAddress.getLocalHost().getHostAddress());
							new LobbyClient();
							parent.dispose();
							break;
						case 1:
							JOptionPane.showMessageDialog(null, inUserandPass);
							break;
						case 2:
							JOptionPane.showMessageDialog(null, "You have been banned.", "User Banned", JOptionPane.INFORMATION_MESSAGE);
							break;
						case 3:
							JOptionPane.showMessageDialog(null, "This user is already online.", "User Online", JOptionPane.INFORMATION_MESSAGE);
							break;
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		});
		register.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				stage = 2;
				subStage = 0;
			}
		});
		
		this.add(user);
		this.add(pass);
		this.add(login);
		this.add(register);
		
		parent.getRootPane().setDefaultButton(login);
		
		subStage++;
	}
	
	public void removeAllComponents()
	{
		Component[] comps = this.getComponents();
		for(Component c : comps)
			if(c != null)
				if(c instanceof JButton || c instanceof JTextField || c instanceof JCheckBox)
					this.remove(c);
	}
	
	public boolean checkUpdate()
	{
		try
		{
			int caught;
			client = new Socket(UserPass.IP_ADDRESS,10003);
			InputStream input = client.getInputStream();
			OutputStream output = client.getOutputStream();
			output.write(MD5Hash.hash(new File(System.getProperty("user.home")+System.getProperty("file.separator")+"JJGame"+System.getProperty("file.separator")+"GameClient.jar")).getBytes());
			caught = input.read();
			//TODO add closes here?
			if(caught == 1)
				return true;
		}
		catch(Exception e)
		{e.printStackTrace();}
		return false;
	}
	
	public void register()
	{
		username.setSize(150,20);
		password.setSize(150,20);
		repass.setSize(150,20);
		avatar.setSize(150,20);
		hide.setSize(75,20);
		regButton.setSize(75,20);
		logButton.setSize(75, 20);
		mainButton.setSize(75, 20);
		
		username.setLocation(13, 100);
		username.setBounds(username.getX(), username.getY(), (int)username.getSize().getWidth(), (int)username.getSize().getHeight());
		
		password.setLocation((int)(username.getX()+username.getSize().getWidth()+50), username.getY());
		password.setBounds(password.getX(), password.getY(), (int)password.getSize().getWidth(), (int)password.getSize().getHeight());
		
		repass.setLocation(username.getX(), (int)(username.getY()+username.getSize().getHeight()+50));
		repass.setBounds(repass.getX(), repass.getY(), (int)repass.getSize().getWidth(), (int)repass.getSize().getHeight());
		
		avatar.setLocation(password.getX(), (int)(password.getY()+password.getSize().getHeight()+50));
		avatar.setBounds(avatar.getX(), avatar.getY(), (int)avatar.getSize().getWidth(), (int)avatar.getSize().getHeight());
		
		hide.setLocation((int)(repass.getX()+(repass.getSize().getWidth())-(GAP*3)), repass.getY()+50);
		hide.setBounds(hide.getX(), hide.getY(), (int)hide.getSize().getWidth(), (int)hide.getSize().getHeight());
		
		regButton.setLocation(hide.getX(), hide.getY()+50);
		regButton.setBounds(regButton.getX(), regButton.getY(), (int)regButton.getSize().getWidth(), (int)regButton.getSize().getHeight());
		
		logButton.setLocation((int)(regButton.getX()-regButton.getSize().getWidth()-GAP), regButton.getY());
		logButton.setBounds(logButton.getX(), logButton.getY(), (int)logButton.getSize().getWidth(), (int)logButton.getSize().getHeight());
		
		mainButton.setLocation((int)(regButton.getX()+regButton.getSize().getWidth()+GAP), regButton.getY());
		mainButton.setBounds(mainButton.getX(), mainButton.getY(), (int)mainButton.getSize().getWidth(), (int)mainButton.getSize().getHeight());
		
		avatar.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				new FileChooser(me);
			}
		});
		regButton.addActionListener(new ActionListener()
		{
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0)
			{
				String userName = username.getText();
				String passWord = password.getText();
				//does some client sided checks before proceeding to send your wanted username to the server
				if(!passWord.equals(repass.getText()))
					JOptionPane.showMessageDialog(null, inPassword);
				else
				{
					if(userName.contains(" ") || passWord.contains(" "))
						JOptionPane.showMessageDialog(null, parseError);
					else
					{
						try
						{
							String ip = InetAddress.getLocalHost().getHostAddress();
							Login b = new Login(new CredenDatagram(userName,passWord,ip,myAva,true));
							b.start();
							b.join();
							if(b.isReg() == false)
								JOptionPane.showMessageDialog(null, takenUsername);
							else
							{
								JOptionPane.showMessageDialog(null, "Registration Finished! You may now login.");
								stage = 1;
								subStage = 0;
								user.setText(userName);
								pass.setText(passWord);
								return;
							}
						} 
						catch (Exception e1)
						{e1.printStackTrace();}
					}
				}
			}
		});
		logButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				stage = 1;
				subStage = 0;
			}
		});
		mainButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				stage = 0;
				subStage = 0;
			}
		});
		
		
		this.add(username);
		this.add(password);
		this.add(repass);
		this.add(avatar);
		this.add(hide);
		this.add(regButton);
		this.add(logButton);
		this.add(mainButton);
		
		subStage++;
	}
	
	public void paintComponent(Graphics g)
	{
		drawRect(0,0,allRec,g);
		g.setColor(Color.black);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		//g.setColor(Color.white);
		//TODO make a better background design?
		//for(int i = 0; i < 15; i++)
		//{
			//a = new StarPolygon(gen.nextInt(this.getWidth())+5,gen.nextInt(this.getHeight())+5, 2,1,6);
			//g.drawPolygon(a);
		//}
		g.setColor(Color.red);
		g.setFont(new Font("Arial Black",Font.ITALIC,16));
		g.drawString("Jared and Jason's", log.x, 60);
		g.drawString("Game!", log.x+50, 75);
		if(stage == 0)
		{
			if(subStage == 0)
			{
				removeAllComponents();
				subStage++;
			}
			g.setFont(new Font("Times New Roman",Font.PLAIN,25));
			drawRect(log,g);
			g.drawString("Login", log.x+50, log.y+20);
			drawRect(reg,g);
			g.drawString("Register", reg.x+40, reg.y+20);
		}
		else
		{
			if(stage == 1)
			{
				if(subStage == 0)
				{
					removeAllComponents();
					unhideFields();
				}
				g.setColor(Color.red);
				g.setFont(new Font("Times New Roman",Font.PLAIN,16));
				g.drawString("Username: ", 50, 150);
				g.drawString("Password: ",50, 180);
			}
			else
			{
				if(stage == 2)
				{
					if(subStage == 0)
					{
						removeAllComponents();
						register();
					}
					g.setColor(Color.red);
					g.setFont(new Font("Times New Roman",Font.PLAIN,16));
					g.drawString("Username: ", username.getX(), username.getY()-GAP);
					g.drawString("Password: ", password.getX(), password.getY()-GAP);
					g.drawString("Re-Type Password: ", repass.getX(), repass.getY()-GAP);
				}
			}
		}
		super.repaint();
	}
	
	public void setAvatar(ImageIcon image)
	{this.myAva = image;}
	
	@Override
	public void mouseClicked(MouseEvent e)
	{
		if(stage == 0)
		{
			if(log.contains(e.getPoint()))
			{
				stage = 1;
				subStage = 0;
			}
			else
			{
				if(reg.contains(e.getPoint()))
				{
					stage = 2;
					subStage = 0;
				}
			}
		}
	}
	public void mouseEntered(MouseEvent arg0)
	{}
	public void mousePressed(MouseEvent arg0)
	{}
	public void mouseReleased(MouseEvent arg0)
	{}
	public void mouseExited(MouseEvent arg0)
	{}

	@Override
	public void keyPressed(KeyEvent e)
	{}
	public void keyReleased(KeyEvent arg0)
	{}
	public void keyTyped(KeyEvent arg0)
	{}

}
