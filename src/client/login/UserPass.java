package client.login;

import global.Player;

import java.awt.*;

import javax.swing.*;
import javax.swing.text.DefaultCaret;

import client.message.IncomingServer;

public class UserPass
{
	private JFrame frame = new JFrame("Game Client");
	private JPanel panel = new JPanel();
	private ContentPanel content = new ContentPanel(frame,this);
	private IncomingServer not = new IncomingServer();
	
	private double version = 1.0;
	
	private JTextArea area = new JTextArea();
	private JScrollPane pane;
	
	public static String IP_ADDRESS;   //static ip address that is used in a lot of places
	public volatile static Player PLAYER; //your player that is synched throughout all threads (not currently used in anything)
	public static boolean usingJar;
	
	/*
	 *   the /ban command
	 *   the getstats frame always gets owner (FIXED)
	 */
	
	public void setFrame()
	{
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setPreferredSize(new Dimension(400,500));
		panel.setPreferredSize(new Dimension(400,500));
		content.setPreferredSize(new Dimension(375,350));
		
		panel.setOpaque(true);
		
		area.setPreferredSize(new Dimension(375,75));
		area.setEditable(false);
		pane = new JScrollPane(area);
		
		area.setWrapStyleWord(true);
		area.setLineWrap(true);
		((DefaultCaret)area.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		panel.add(content);
		panel.add(pane);
		
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		not.start();
		
		log("Client Build Alpha: "+version);
		log("Server I.P. address: "+UserPass.IP_ADDRESS);
		log("Location: "+System.getProperty("user.home")+System.getProperty("file.separator")+"JJGame");
	}
	
	public void log(String text)
	{
		if(area.getText().length() == 0)
			area.setText(text);
		else
			area.setText(area.getText()+"\n"+text);
	}
	
	public void setPlayer(Player set)
	{
		UserPass.PLAYER = set;
	}
	
	public static void main(String[] args)
	{
		if(args.length == 2)
		{
			UserPass.IP_ADDRESS = args[0];
			usingJar = Boolean.parseBoolean(args[1]);
		}
		else
		{
			if(args.length == 1)
			{
				try
				{
					usingJar = Boolean.parseBoolean(args[0]);
				}
				catch(Exception e)
				{System.out.println("First argument is not a boolean"); UserPass.IP_ADDRESS = args[0]; usingJar = false;}
			}
			else
			{
				if(args.length == 0)
				{
					UserPass.IP_ADDRESS = "localhost";
					usingJar = false;
				}
			}
		}
		UserPass a = new UserPass();
		a.setFrame();
	}

}
