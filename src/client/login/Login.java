package client.login;

import java.io.*;
import java.net.Socket;

import global.dgframework.CredenDatagram;
import global.dgframework.Datagram;
import global.dgframework.DatagramHandler;

public class Login extends Thread
{
	private Socket client;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	
	private CredenDatagram info;
	private CredenDatagram ret;
	
	public Login(CredenDatagram info)
	{
		this.info = info;
	}
	
	public void run()
	{
		try
		{
			client = new Socket(UserPass.IP_ADDRESS, 5000);
			out = new ObjectOutputStream(client.getOutputStream());
			in = new ObjectInputStream(client.getInputStream());
			out.writeObject(info);
			ret = (CredenDatagram)in.readObject();
			out.close();
			in.close();
			client.close();
		}
		catch(Exception e)
		{System.err.println("Error in Connecting to Authentication Server");}
	}
	
	public int valid()
	{
		if(ret.isOnline())
			return 3;
		else
			if(ret.isBan())
				return 2;
			else
				if(!ret.isReg())
					return 1;
				else
					return 0;
	}
	
	public boolean isReg()
	{
		return ret.isReg();
	}

}
