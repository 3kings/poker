package client.login;
import java.awt.Polygon;

 public class StarPolygon extends Polygon
 {
	 public StarPolygon(int x, int y, int r, int innerR, int vertexCount)
	 {
		 this(x, y, r, innerR, vertexCount, 0.0D); }
	 		public StarPolygon(int x, int y, int r, int innerR, int vertexCount, double startAngle) {
/* 10 */     super(getXCoordinates(x, y, r, innerR, vertexCount, startAngle), getYCoordinates(x, y, r, innerR, vertexCount, startAngle), vertexCount * 2);
/*    */   }
/*    */ 
/*    */   protected static int[] getXCoordinates(int x, int y, int r, int innerR, int vertexCount, double startAngle)
/*    */   {
/* 16 */     int[] res = new int[vertexCount * 2];
/* 17 */     double addAngle = 6.283185307179586D / vertexCount;
/* 18 */     double angle = startAngle;
/* 19 */     double innerAngle = startAngle + 3.141592653589793D / vertexCount;
/* 20 */     for (int i = 0; i < vertexCount; ++i) {
/* 21 */       res[(i * 2)] = ((int)Math.round(r * Math.cos(angle)) + x);
/* 22 */       angle += addAngle;
/* 23 */       res[(i * 2 + 1)] = ((int)Math.round(innerR * Math.cos(innerAngle)) + x);
/* 24 */       innerAngle += addAngle;
/*    */     }
/* 26 */     return res;
/*    */   }
/*    */ 
/*    */   protected static int[] getYCoordinates(int x, int y, int r, int innerR, int vertexCount, double startAngle) {
/* 30 */     int[] res = new int[vertexCount * 2];
/* 31 */     double addAngle = 6.283185307179586D / vertexCount;
/* 32 */     double angle = startAngle;
/* 33 */     double innerAngle = startAngle + 3.141592653589793D / vertexCount;
/* 34 */     for (int i = 0; i < vertexCount; ++i) {
/* 35 */       res[(i * 2)] = ((int)Math.round(r * Math.sin(angle)) + y);
/* 36 */       angle += addAngle;
/* 37 */       res[(i * 2 + 1)] = ((int)Math.round(innerR * Math.sin(innerAngle)) + y);
/* 38 */       innerAngle += addAngle;
/*    */     }
/* 40 */     return res;
/*    */   }
/*    */ }