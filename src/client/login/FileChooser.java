package client.login;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class FileChooser extends Thread
{
	JFrame frame = new JFrame();
	JPanel panel = new JPanel();
	JFileChooser a = new JFileChooser();
	boolean waiting = false;
	ContentPanel parent;
	
	/**
	 * Accesses the setAvatar(ImageIcon image) in the Register class
	 * @param parent sent the Register class to set the avatar when one is chosen
	 */
	public FileChooser(final ContentPanel parent)
	{
		super("FileChooser");
		this.parent = parent;
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.setPreferredSize(new Dimension(500,500));
		panel.setPreferredSize(new Dimension(500,500));
		a.setPreferredSize(new Dimension(400,400));
		
		
		a.addChoosableFileFilter(new ImageFilter());
		a.setAcceptAllFileFilterUsed(false);
		
		a.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				parent.setAvatar(getSelection());
				frame.dispose();
			}
		});
		
		panel.add(a);
		
		waiting = true;
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setVisible(true);
		start();
	}
	
	//Converts the files path into an ImageIcon
	public ImageIcon getSelection()
	{
		return new ImageIcon(a.getSelectedFile().getAbsolutePath());
	}

	public boolean isWaiting()
	{
		return waiting;
	}
}
