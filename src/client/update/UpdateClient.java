package client.update;

import global.MD5Hash;
import global.dgframework.VerifyDatagram;

import java.awt.Dimension;
import java.awt.event.*;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.*;

import client.information.Information;

public class UpdateClient
{
	private static Socket update = null;
	
	private JFrame frame = new JFrame("Updater");
	private JPanel panel = new JPanel();
	private JTextField field = new JTextField("Please enter i.p. here!");
	private JButton button = new JButton("Connect");
	private JLabel label = new JLabel("Please enter the i.p. below");
	
	private String pathToFolder;
	private String pathToJar;
	
	//TODO Make a recent I.P. address list
	
	private String pathToIpFile; 
	
	private String ip;
	
	public UpdateClient()
	{
		pathToFolder = System.getProperty("user.home")+System.getProperty("file.separator")+"JJGame";
		pathToJar = System.getProperty("user.home")+System.getProperty("file.separator")+"JJGame"+System.getProperty("file.separator")+"GameClient.jar";
	}
	
	public void setFrame()
	{
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setPreferredSize(new Dimension(200,150));
		panel.setPreferredSize(new Dimension(200,150));
		field.setPreferredSize(new Dimension(175,20));
		
		panel.add(label);
		panel.add(field);
		panel.add(button);
		
		field.addFocusListener(new FocusListener()
		{
			public void focusGained(FocusEvent arg0)
			{
				field.setText("");
			}
			public void focusLost(FocusEvent arg0)
			{
			}
		});
		field.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				button.doClick();
			}
		});
		button.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				try
				{
					ip = field.getText();
					if(ip.length() == 0)
						ip = "localhost";
					File folder = new File(pathToFolder);
					if(!folder.exists())
						folder.mkdir();
					File jarclient = new File(pathToJar);
					boolean update = hasUpdate(jarclient);
					if(update)
						getUpdate(ip);
					Runtime.getRuntime().exec("java -jar "+pathToJar+" "+ip+" true");
					frame.dispose();
				}
				catch(Exception e)
				{e.printStackTrace();}
			}
		});
		
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setVisible(true);
	}
	
	//runs to check if there are updates
	public static void main(String[] args) throws Exception
	{
		UpdateClient up = new UpdateClient();
		up.setFrame();
	}
	
	/**
	 * Send the GameClient.jar to this method and it will check server-sided comparing hashes
	 * @param f GameClient.jar
	 * @return true if there is an update false if there is not
	 * @throws Exception
	 */
	public boolean hasUpdate(File f) throws Exception
	{
		if(!f.exists())
			return true;
		Information a = new Information(new VerifyDatagram(MD5Hash.hash(f).getBytes()));
		a.start();
		try
		{
			a.join();
		}
		catch(Exception e)
		{e.printStackTrace();}
		return ((VerifyDatagram)a.getDatagram()).getUpdate() == 1;
	}
	
	/**
	 * Connects to the server and receives the new up to date GameClient.jar will save it in the directory of the play.bat file
	 * @param ip The i.p. of the server to connect to
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public void getUpdate(String ip) throws UnknownHostException, IOException
	{
		update = new Socket(ip,10004);
		BufferedInputStream is = new BufferedInputStream(update.getInputStream());
		BufferedOutputStream os = new BufferedOutputStream(update.getOutputStream());
		
		File updated = new File(pathToJar);
		if(updated.exists())
			updated.delete();
		updated.createNewFile();
		
		BufferedOutputStream osf = new BufferedOutputStream(new FileOutputStream(updated));
		
		byte[] buffer = new byte[1024];
		int numRead = 0;
		while((numRead = is.read(buffer)) != -1)
			osf.write(buffer, 0, numRead);
		
		is.close();
		os.close();
		update.close();
		osf.close();
	}
}
