@echo off
mkdir build
javac -sourcepath src -d build C:\Users\DSU\workspace\poker\src\client\login\*.java
rmdir /s /q "C:\Users\DSU\workspace\poker\build\client"
cd C:\Users\DSU\workspace\poker\build
@echo on
mkdir META-INF
cd META-INF
echo Manifest-Version: 1.0 >> MANIFEST.MF
echo Main-Class: server.mainserver.MainServer >> MANIFEST.MF
echo Compiled class files now building jar file...
cd C:\Users\DSU\workspace\poker\build
jar cfm C:\Users\DSU\workspace\poker\GameServer.jar META-INF\MANIFEST.MF server\ global\
echo Build complete... Saved as GameServer.jar
cd C:\Users\DSU\workspace\poker
rmdir /s /q "C:\Users\DSU\workspace\poker\build"
cls