@echo off
mkdir build
javac -sourcepath src -d build C:\Users\DSU\workspace\poker\src\client\update\UpdateClient.java
javac -sourcepath src -d build C:\Users\DSU\workspace\poker\src\global\MD5Hash.java
cd C:\Users\DSU\workspace\poker\build
@echo on
mkdir META-INF
cd META-INF
echo Manifest-Version: 1.0 >> MANIFEST.MF
echo Main-Class: client.update.UpdateClient >> MANIFEST.MF
echo Compiled class files now building jar file...
cd C:\Users\DSU\workspace\poker\build
jar cfm C:\Users\DSU\workspace\poker\Updater.jar META-INF\MANIFEST.MF client\ global\
echo Build complete... Saved as Updater.jar
cd C:\Users\DSU\workspace\poker
rmdir /s /q "C:\Users\DSU\workspace\poker\build"
cls