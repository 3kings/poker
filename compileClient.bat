@echo off
mkdir build
javac -sourcepath src -d build C:\Users\DSU\workspace\poker\src\client\login\*.java
rmdir /s /q "C:\Users\DSU\workspace\poker\build\server"
cd C:\Users\DSU\workspace\poker\build
@echo on
mkdir META-INF
cd META-INF
echo Manifest-Version: 1.0 >> MANIFEST.MF
echo Main-Class: client.login.UserPass >> MANIFEST.MF
echo Compiled class files now building jar file...
cd C:\Users\DSU\workspace\poker\build
mkdir cards
cd C:\Users\DSU\workspace\poker
xcopy /e /y cards C:\Users\DSU\workspace\poker\build\cards
cd C:\Users\DSU\workspace\poker\build
jar cfm C:\Users\DSU\workspace\poker\GameClient.jar META-INF\MANIFEST.MF client\ global\ cards\
echo Build complete... Saved as GameClient.jar
cd C:\Users\DSU\workspace\poker
rmdir /s /q "C:\Users\DSU\workspace\poker\build"
cls